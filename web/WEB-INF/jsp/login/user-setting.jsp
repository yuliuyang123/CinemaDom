
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<base href="<%=basePath %>" />
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
    .layui-form-item .layui-input-company {width: auto;padding-right: 10px;line-height: 38px;}
</style>
<link rel="stylesheet" href="static/layuimini-v2-onepage/lib/jq-module/zyupload/zyupload-1.0.0.min.css" media="all">
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main" style="height: 770px">

        <div class="layui-form layuimini-form">
            <div class="layui-form-item">
                <label class="layui-form-label ">姓名</label>
                <div class="layui-input-block">
                    <input type="hidden" name="uid" lay-verify="required"
                           value="${user.uid}" class="layui-input" readonly="readonly" >
                    <input type="text" name="uname" lay-verify="required"
                           value="${user.uname}" class="layui-input" disabled="disabled"style="border:none;">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">图片上传</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="test1">
                        <i class="layui-icon">&#xe67c;</i>上传图片
                    </button>
                    <img id="imgPathBro" name="imgPath" src="http://localhost:8091/img/${user.avatar}" style="width: 100px;">
                    <input id="Avatar" type="hidden" name="Avatar">


                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">性别</label>
                <div class="layui-input-block">
                        <input type="text" name="sex" class="layui-input" value="${user.sex}" disabled="disabled"style="border:none;"  >
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">用户名</label>
                <div class="layui-input-block">
                    <input type="text" name="username" lay-verify="required" lay-reqtext="用户名不能为空" placeholder="请输入用户名"
                           value="${user.username}" class="layui-input"disabled="disabled"style="border:none;" >
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">密码</label>
                <div class="layui-input-block">
                    <input type="text" name="password" lay-verify="required" lay-reqtext="密码不能为空" placeholder="请输入密码"
                           value="${user.password}" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">手机号</label>
                <div class="layui-input-block">
                    <input type="number" name="phone" lay-verify="required" lay-reqtext="手机不能为空" placeholder="请输入手机"
                           value="${user.phone}" class="layui-input"style="border:none;">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">身份证</label>
                <div class="layui-input-block">
                    <input type="text" name="idcard" placeholder="请输入" value="${user.idcard}" class="layui-input" disabled="disabled"style="border:none;">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    layui.use(['form','miniPage'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.jquery,
            miniPage = layui.miniPage;

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();
        $(function () {
            //alert("${user.avatar}")
           $("#imgPath").val("${user.avatar}");
        })
        //监听提交
        form.on('submit(saveBtn)', function (data) {

            $.post("user/update", data.field, function (data) {
                if (data.success) {
                    layer.msg('修改成功，正在退出', function () {
                        window.location = 'login';
                    });
                } else {
                    layer.msg(data.error);
                }
                return false;
            });
        });
    });
</script>
<script src="static/layuimini-v2-onepage/lib/jquery-3.4.1/jquery-3.4.1.min.js" charset="utf-8"></script>
<script src="static/layuimini-v2-onepage/lib/jq-module/zyupload/zyupload-1.0.0.min.js" charset="utf-8"></script>
<script type="text/javascript">
    layui.use(['upload', 'element', 'layer'], function(){
        var $ = layui.jquery
            ,upload = layui.upload
            ,element = layui.element
            ,layer = layui.layer;
        //绑定图片上传
        var uploadInst = upload.render({
            elem: '#test1' //绑定元素
            , url: 'user/upload' //上传接口
            , done: function (res) {
                //上传完毕回调
                console.info("上传完成", res.target);
                $("#Avatar").val(res.target);
                //更改预览图片标签的src属性
                $("#imgPathBro").attr("src", "http://localhost:8091/img/" + res.target)
            }
            , error: function () {
                //请求异常回调
            }
        });
    });

</script>