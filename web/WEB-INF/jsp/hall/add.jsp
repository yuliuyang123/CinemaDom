<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2022/3/12
  Time: 22:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/buySeat.css" media="all">

<div class="layuimini-main" style=" height: 700px; ">
    <div class="layui-form layuimini-form" lay-filter="userForm"
         style=" height: 700px  ;width:700px;float: left; ">
        <div class="layui-form-item" style="margin-top: 50px">
            <label class="layui-form-label required">影厅名</label>
            <div class="layui-input-block">
                <input id="rid" style="width:500px" type="hidden" name="id"/>
                <input type="text" style="width:550px" name="hname" lay-verify="required|hname" lay-reqtext="影厅名不能为空"
                       placeholder="请输入影厅名" value="" class="layui-input">
            </div>
        </div>
        <div style="margin-left: 37px;width: 500px; margin-top:30px; ">
            <label class="layui-form-label required">行和列</label>
            <input type="text" id="hang" name="hang" style="width: 70px;height: 37px" lay-verify="required"/>&nbsp;&nbsp;行 &nbsp; &nbsp;
            &nbsp; &nbsp;
            <input type="text" id="lie" name="lie" style="width: 70px;height: 37px" lay-verify="required"/>&nbsp;&nbsp;列
        </div>
        <br><br><br>
        <button class="layui-btn layui-btn-normal" style="margin-left: 90px" data-method="offset" data-type="auto" class="layui-btn layui-btn-normal" id="addSeate">生成座位
        </button>
        <%--<div style="margin-top: -150px;"></div>--%>
        <br><br><br><br>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">影厅介绍</label>
            <div class="layui-input-block">
                <textarea name="detailed" placeholder="请输入内容" class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item " style="margin-top: 100px">
            <div class="layui-input-block">
                <button style="margin-left: 130px" class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确定保存</button>
            </div>
        </div>
    </div>
    <div class="layui-form layuimini-form" lay-filter="userForm"
         style=" height: 700px  ;width:700px;float: right; margin-right: 30px">
        <div class="main" style=" float: right;    height:650px; width: 650px; margin-top: 30px">
            <!-- 主页 -->
            <div class="hall">
                <div class="seat-example">
                    <div class="selectable-example example">
                        <span>可用座位</span>
                    </div>
                    <div class="disabled-example example">
                        <span>禁用座位</span>
                    </div>
                </div>
                <div class="seats-block">
                    <div class="row-id-container">
                    </div>
                    <div class="seats-container">
                        <div class="screen-container">
                            <div class="screen">银幕中央</div>
                        </div>
                        <div class="seats-wrapper" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>



    layui.use(['form', 'table', 'tree'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            tree = layui.tree,
            $ = layui.$;
        $(".seats-wrapper").on('click','.seat',function(){
            var row = $(this).attr('row');
            var col = $(this).attr('col');
            alert(row+'|'+col);
            if($(this).is('.disabled')){
                $(this).removeClass('disabled');
            }else{
                $(this).addClass('disabled');
            }
            // $(this).attr("style","visibility:hidden");
            // $(this).attr("visibility","hidden");
        });


        $("#addSeate").click(function(){
            var hang = $("#hang").val();
            var lie = $("#lie").val();
            $(".row-id-container").empty();
            $(".seats-wrapper").empty();
            if(hang>12||lie>12){
                layer.msg("行列超过影厅最大限制");
            }else if((hang===null||lie===null)||(hang===""||lie==="")){
                layer.msg("行列不能为空");
            }else
            initSeat(hang,lie);
        });
        function initSeat(hang, lie) {
            var $rowIdContainer = $(".row-id-container");
            for(var i=1;i<=hang;i++){
                $('<span class="row-id">').text(i).appendTo($rowIdContainer);
            }
            var $seat = $(".seats-wrapper");
            for(var i=1;i<=hang;i++){
                var $row = $('<div class="row">').appendTo($seat);
                for (var j = 1; j <= lie; j++) {
                    var $col = $('<span class="seat">').appendTo($row);
                    $col.attr('row',i+"排").attr('col',j+"座")
                }
            }
        }
        //点击具体座位事件

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();

        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            //
            // var index = layer.alert(JSON.stringify(data.field), {
            //      title: '最终的提交信息'
            //  }, function () {
            //
            //
            //
            //  });
            var seat = data.field.hang*data.field.lie;
            data.field.seat=seat

            $.post("hall/saveOrUpdate",data.field,function (data) {
                if(data.success){
                    // 关闭弹出层
                    layer.close(parentIndex);
                    //刷新界面
                    table.reload("currentTableId");
                }else{
                    layer.msg(data.error);
                }
            },"json")
            return false;
        });


        form.verify({
            hname: function(value, item){
                var info;
                var id=$('#rid').val();
                $.ajax({
                    url:"hall/checkName",
                    async:false,
                    data:{hname:value,hid:id},
                    success:function (data) {
                        console.info(data);
                        if (data.success){
                            info="";
                        } else {
                            info="电影名重复";
                        }
                    }
                } );
                return info;
            }
        });

    });
</script>
