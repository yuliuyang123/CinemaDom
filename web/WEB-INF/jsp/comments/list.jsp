<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/3/18
  Time: 9:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/html" id="movie1">
    <span style="color: red;"> {{d.film.chinese_name}}</span>
</script>
<script type="text/html" id="user">
    <span style="color: red;"> {{d.leadingUser.luname}}</span>
</script>
<%--<script type="text/html" id="reception1">--%>
<%--<span style="color: red;"> {{d.reception.rname}}</span>--%>
<%--</script>--%>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">

        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">前台用户名称</label>
                            <div class="layui-input-inline">
                                <input type="text" name="keyword" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>

                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>


    </div>
</div>
<script>
    layui.use(['form', 'table','miniPage','element'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;

        table.render({
            elem: '#currentTableId',
            url: 'comments/listData',
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 110, title: '评价编号', sort: true},
                {width: 150, title: '前台用户名',templet:"#user"},
                { field: 'film',width: 120, title: '影片名称', sort: true,templet:"#movie1"},
                {field: 'content', width: 180, title: '评价内容', sort: true},
                {field: 'create_time', width: 150, title: '评价时间', sort: true},
                {title: '操作', minWidth: 150, toolbar: '#currentTableBar', align: "center"}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            page: true,
            skin: 'line'
        });
        //刷新界面
        $("#shuaxin").bind("click",function () {
            window.location.reload();
        });
        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            //执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: data.field
            }, 'data');

            return false;
        });

        table.on('tool(currentTableFilter)', function (obj) {
            layer.confirm('是否确认删除?', {icon: 3, title:'提示'}, function(index){
                //往后台发送ajax请求 删除数据
                $.post("comments/delete",{id:obj.data.id},function (data) {
                    // alert($)
                    console.info(data);
                    if(data.success){
                        layer.close(index);
                        //刷新界面
                        table.reload("currentTableId");
                    }else{
                        layer.msg(data.error);
                    }

                })

            });
        });

    });
</script>
