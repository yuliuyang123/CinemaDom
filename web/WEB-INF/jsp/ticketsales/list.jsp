<%--
  Created by IntelliJ IDEA.
  User: 郭鹏飞
  Date: 2022/3/13
  Time: 2:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/html" id="FilmTpl">
    <span > {{d.film.chinese_name}}</span>
</script>
<script type="text/html" id="HallTpl">
    <span > {{d.hall.hname}}</span>
</script>
<script type="text/html" id="imgTpl">
    <img width="50"  src="http://localhost:8081/img/{{d.imgUrl}}">
</script>
<style type="text/css">

    .layui-table-cell {
        height: auto;
        line-height: 60px;
    }
</style>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">

        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">电影名</label>
                            <div class="layui-input-inline">
                                <input type="text" id="filmSelect1" name="keyword" autocomplete="off" class="layui-input">
                            </div>
                        </div>

                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> 添加 </button>
                <button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" lay-event="delete"> 删除 </button>
            </div>
        </script>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>

    </div>
</div>

<script>
    layui.use(['form', 'table','miniPage','element','upload'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;

        table.render({
            elem: '#currentTableId',
            url: 'ticketSales/listData',
            toolbar: '#toolbarDemo',
            page:true,
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'tid', width: 80, title: 'ID', sort: true},
                {title: '电影名',sort: true,templet:"#FilmTpl"},//外键
                {title: '影厅',templet:"#HallTpl"},//外键
                {field: 'opening_time', title: '放映时间'},
                {field: 'break_time', title: '结束时间'},
                {field: 'price',  title: '价格', sort: true},
                {field: 'creation_time',  title: '创建时间', sort: true},
                {field: 'remaining_votes',  title: '剩余票数', sort: true},
                {title: '操作', minWidth: 100, toolbar: '#currentTableBar', align: "center"}

            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            //执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: data.field
            }, 'data');

            return false;
        });

        /**
         * toolbar事件监听
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'add') {   // 监听添加操作
                var content = miniPage.getHrefContent('ticketSales/toEdit');
                var openWH = miniPage.getOpenWidthHeight();

                var index = layer.open({
                    title: '添加场次',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ["400px","400px"],
                    offset: ["200px"],
                    content: content,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            } else if (obj.event === 'delete') {  // 监听删除操作
                var checkStatus = table.checkStatus('currentTableId')
                    , data = checkStatus.data;
                layer.alert(JSON.stringify(data));
            }
        });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            console.info(obj.data)
            if (obj.event === 'edit') {
                var content = miniPage.getHrefContent('movie/toEdit');
                var openWH = miniPage.getOpenWidthHeight();
                var index = layer.open({
                    title: '编辑电影',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content
                });
                console.info(obj.data);
                form.val("testForm",obj.data);
                console.info("要选择的类别的id为",obj.data.mtype.tid);
                $("#mtypeSelect").val(obj.data.mtype.tid);
                //更新下拉列表渲染
                form.render("select");
                var imgPath = obj.data.avatar;
                console.info(imgPath)
                //给预览图片的 标签的src属性赋值
                $("#imgPathBro").attr("src","http://localhost:8091/img/"+imgPath);

                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('真的删除行么', function (index) {
                    $.post("ticketSales/delete",{tid:obj.data.tid},function (data) {
                        console.info(data);
                        if (data.success) {
                            layer.close(index);
                            //刷新数据
                            table.reload('currentTableId');
                        } else {
                            layer.msg(data.error);
                        }
                    })
                });
            }
        });

        $(function () {
            $.ajax({
                url: 'ticketSales/listAllFilm',
                dataType: 'json',
                type:'post',
                async:false,
                success: function (data) {
                    $.each(data, function (index, item) {
                        // console.log("item",item);
                        //option 第一个参数是页面显示的值，第二个参数是传递到后台的值
                        $('#filmSelect1').append(new Option(item.chinese_name, item.id));//往下拉菜单里添加元素
                        //设置value（这个值就可以是在更新的时候后台传递到前台的值）为2的值为默认选中
                        //$('#selectId').val(2);
                    })
                    //form.render(); //更新全部表单内容
                    form.render("select"); //刷新表单select选择框渲染
                }
            });
        });
    });
</script>