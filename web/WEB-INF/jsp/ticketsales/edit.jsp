<%--
  Created by IntelliJ IDEA.
  User: 郭鹏飞
  Date: 2022/3/15
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<style type="text/css">
    .layui-laydate-content > .layui-laydate-list {
        padding-bottom: 0px;
        overflow: hidden;
    }

    .layui-laydate-content > .layui-laydate-list > li {
        width: 50%
    }

    .merge-box .scrollbox .merge-list {
        padding-bottom: 5px;
    }

</style>

<form id="pForm" class="layui-form" action="" lay-filter="testForm" style="width: 80%;margin-top: 20px;">
    <div class="layui-form-item">
        <label class="layui-form-label">电影</label>
        <div class="layui-input-block">
            <select id="filmSelect" name="film.id" lay-filter="film" lay-verify="required">
                <option value="">请选择</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">影厅</label>
        <div class="layui-input-block">
            <select id="hallSelect" name="hall.hid" lay-filter="hall" lay-verify="hallVerify">
                <option value="">请选择</option>
            </select>
        </div>
    </div>

    <div class="layui-inline">
        <label class="layui-form-label">放映时间</label>
        <div class="layui-input-inline" id="fangyingshijian">
            <input disabled type="text" name="opening_time" id="date" lay-verify="opening_time"
                   placeholder="yyyy-MM-dd HH:mm" autocomplete="off" class="layui-input">
            <br>
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">结束时间</label>
        <div class="layui-input-inline">
            <input disabled type="text" name="break_time" id="jieshushijian" lay-verify="datetime"
                   placeholder="设置放映时间自动获取 " autocomplete="off" class="layui-input">
            <br>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">价格</label>
        <div class="layui-input-block">
            <input type="text" name="price" required lay-verify="price" placeholder="请输入价格" autocomplete="off"
                   class="layui-input">
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    //当前电影的时长
    var duration = null;
    //当前影厅的座位(拍片的票数)
    var remaining_votes = null;
    layui.use(['form', 'table', 'upload', 'layedit', 'laydate'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            upload = layui.upload,
            $ = layui.$
            , layedit = layui.layedit
            , laydate = layui.laydate;



        form.verify({
            opening_time: function(value, item){ //value：表单的值、item：表单的DOM对象
               // alert(value)
                var info ;
                var hid=$('#hallSelect').val();
                var break_time=$('#jieshushijian').val();
                //往后台发送ajax请求判断用户名是否重复
                $.ajax({
                    url:"ticketSales/checkSales",
                    async: false,
                    data:{hid:hid,opening_time:value,break_time:break_time},
                    type: "POST",
                    success: function(data) {
                        console.info(data.success)
                        if(data.success){
                            info="";
                        }else{
                            info="场次时间冲突";
                        }
                    }
                });
                return info;

            }
        });

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();

        $('body').on('click', "#fangyingshijian", function () {
            var disabled = $("#date").attr('disabled');
            if (disabled) {
                layer.msg("请先选择电影");
            }
        });


        /**
         * 时间控件
         */
        laydate.render({
            elem: '#date'  // 控件触发的标签
            , type: 'datetime'
            , format: 'yyyy-MM-dd HH:mm'
            , min: new Date().toLocaleString()//不能小于当前时间
            , trigger: 'click'	  // 事件类型
            , done: function (value) {
                // 回调函数
                var now = new Date(value);
                var m = parseInt(duration);
                var datetime = new Date(now.setMinutes(now.getMinutes() + m));
                //alert(datetime)
                var year = datetime.getFullYear();
                var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
                var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
                var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
                var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
                var jieshushijian = year + "-" + month + "-" + date + " " + hour + ":" + minute;
                $("#jieshushijian").val(jieshushijian);
            }
        });


        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;


        $.ajax({
            url: 'ticketSales/listFilm',
            dataType: 'json',
            type: 'post',
            async: false,
            success: function (data) {
                $.each(data, function (index, item) {
                    //option 第一个参数是页面显示的值，第二个参数是传递到后台的值
                    $('#filmSelect').append(new Option(item.chinese_name, item.id));//往下拉菜单里添加元素
                    //设置value（这个值就可以是在更新的时候后台传递到前台的值）为2的值为默认选中
                    //$('#selectId').val(2);
                })
                form.render("select"); //刷新表单select选择框渲染

            }
        });
        //动态添加下拉框 同时可以设置默认值
        $.ajax({
            url: 'ticketSales/listHall',
            dataType: 'json',
            type: 'post',
            async: false,
            success: function (data) {
                $.each(data, function (index, item) {
                    //option 第一个参数是页面显示的值，第二个参数是传递到后台的值
                    $('#hallSelect').append(new Option(item.hname, item.hid));//往下拉菜单里添加元素
                    //设置value（这个值就可以是在更新的时候后台传递到前台的值）为2的值为默认选中
                    //$('#selectId').val(2);
                })
                //form.render(); //更新全部表单内容
                form.render("select"); //刷新表单select选择框渲染

            }
        });

        // 监听 电影下拉框
        form.on("select(film)", function (data) {
            var id = data.value; // 获取选中的值
            if ($("#filmSelect").val() === "") {
                $("#date").attr("disabled", "disabled");
            } else {
                $("#date").removeAttr("disabled");
            }
            $(" #date").val("");
            $(" #jieshushijian").val("");
            if (id != "") {
                $.post("ticketSales/listFilmByid", {id: id}, function (data) {
                    duration = data.duration;
                }, "json")
            }
        });
        // 监听 影厅下拉框
        form.on("select(hall)", function (data) {
            var hid = data.value; // 获取选中的值

            if (hid != "") {
                $.post("ticketSales/listHallByHid", {hid: hid}, function (data) {
                    remaining_votes = data.seat;
                }, "json")
            }
        });

        //监听提交
        form.on('submit(formDemo)', function (data) {
            // alert($("#filmSelect").val());
            data.field.remaining_votes=remaining_votes
            // layer.msg(JSON.stringify(data.field));


            //往后台发送ajax请求
                $.post("ticketSales/save",data.field, function (data) {
                //如果操作成功
                if (data.success) {
                    // 关闭弹出层
                    layer.close(parentIndex);
                    //刷新界面
                    table.reload("currentTableId");
                } else {
                    layer.msg(data.error);
                }
            })
            return false;
        });

    });
</script>