<%--
  Created by IntelliJ IDEA.
  User: 86181
  Date: 2022/3/14
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<base href="<%=basePath %>" />

<html>
<head>
    <title>商品订单详情列表</title>
</head>

<script type="text/html" id="orderTpl">
    <span style="color: red">{{d.order.order_number}}</span>
</script>
<script type="text/html" id="productTpl">
    <img width="50" height="40" id="imgPathPro"  src="http://localhost:8081/img/{{d.product.img_product}}">  {{d.product.pname}}
</script>
<body>

<table class="layui-hide" id="test"></table>

<script>
    layui.use('table', function(){
        var table = layui.table;

        table.render({
            elem: '#test'
            ,url:'orderdetil/listDetail?oid='+"${oid}"
            ,cols: [[
                {type: "checkbox", width: 50},
                // {field: 'id', width: 150, title: '订单详情ID', sort: true},
                {field: 'oid', title: '订单编号',templet:"#orderTpl"},
                {field: 'pname', width: 200, title: '商品名称',templet:"#productTpl"},
                {field:'price', width:150, title: '价格'},
                {field: 'stock', width: 150, title: '数量'},
                {field: 'subtotal', width: 150, title: '小计'},
            ]]
            // ,page: true
        });
    });
</script>
</body>
</html>
