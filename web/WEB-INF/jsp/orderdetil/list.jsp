<%--
  Created by IntelliJ IDEA.
  User: 86181
  Date: 2022/3/14
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<base href="<%=basePath %>" />

<html>
<head>
    <title>商品订单列表</title>

    <div class="layuimini-container layuimini-page-anim">
        <div class="layuimini-main">

            <fieldset class="table-search-fieldset">
                <legend>搜索信息</legend>
                <div style="margin: 10px 10px 10px 10px">
                    <form class="layui-form layui-form-pane" action="">
                        <div class="layui-form-item">
                            <div class="layui-inline">
                                <label class="layui-form-label">订单编号</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="keyword" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <%--<div class="layui-inline">--%>
                                <%--<label class="layui-form-label">用户名</label>--%>
                                <%--<div class="layui-input-inline">--%>
                                    <%--<input type="text" name="keyword" autocomplete="off" class="layui-input">--%>
                                <%--</div>--%>
                            <%--</div>--%>

                            <div class="layui-inline">
                                <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                            </div>
                        </div>
                    </form>
                </div>
            </fieldset>


            <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

            <script type="text/html" id="currentTableBar">
                <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="detail">订单详情</a>
            </script>

        </div>
    </div>
</head>

<%--<script type="text/html" id="userTpl">--%>
    <%--<span style="color: red">{{d.user.username}}</span>--%>
<%--</script>--%>

<script>
    layui.use(['form', 'table','miniPage','element'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;

        table.render({
            elem: '#currentTableId',
            url: 'orderdetil/listOrder',
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 200, title: '订单ID', sort: true},
                {field: 'order_number',width: 200, title: '订单编号'},
                // {field: 'uid', width: 200, title: '用户名',templet:"#userTpl"},
                {field:'create_date',width: 230, title: '创建时间'},
                {field: 'total', title: '总金额'},
                {title: '操作', minWidth: 150, toolbar: '#currentTableBar', align: "center"}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 10,
            page: true,
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            var result = JSON.stringify(data.field);


            //执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: data.field
            }, 'data');

            return false;
        });


        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            if (obj.event === 'detail') {
                <%--var content = miniPage.getHrefContent('product/toEdit.do?name='+"${name}");--%>
                var content = miniPage.getHrefContent('orderdetil/dList?oid='+obj.data.order_number);
                var openWH = miniPage.getOpenWidthHeight();

                var index = layer.open({
                    title: '订单详情界面',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['1000px',  '600px'],
                    content: content,
                });
                form.val("productFrom",data)

                //获取到当前商品的图片路径
                var imgPath = obj.data.img_product;
                //给预览图片的 标签的src属性赋值
                $("#imgPathBro").attr("src","http://localhost:8081/img/"+imgPath);
                console.info(obj.data)

                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            }
        });


    });
</script>
</body>
</html>
