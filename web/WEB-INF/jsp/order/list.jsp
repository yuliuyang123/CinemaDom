<%--
  Created by IntelliJ IDEA.
  User: 郭鹏飞
  Date: 2022/3/13
  Time: 2:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/html" id="productTpl">
    <img width="50" src="http://localhost:8091/img/{{d.avatar}}">
</script>
<%--电影名--%>
<script type="text/html" id="filmNameTp">
    <span > {{d.ticketSales.film.chinese_name}}</span>
</script>
<%--影厅--%>
<script type="text/html" id="hallNameTp">
    <span > {{d.ticketSales.hall.hname}}</span>
</script>
<%--场次--%>
<script type="text/html" id="changciTp">
    <span > {{d.ticketSales.opening_time}}</span>
</script>
<%--用户--%>
<script type="text/html" id="luIdTp">
    <span > {{d.luid.uname}}</span>
</script>
<%--状态--%>
<script type="text/html" id="stateTp">
    <span style="color: green"> {{d.state.iname}}</span>
</script>
<script type="text/html" id="imgTpl">
    <img width="50"  src="http://localhost:8081/img/{{d.imgUrl}}">
</script>
<style type="text/css">

    .layui-table-cell {
        height: auto;
        line-height: 60px;
    }
</style>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">

        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">商品名称</label>
                            <div class="layui-input-inline">
                                <input type="text" name="keyword" autocomplete="off" class="layui-input">
                            </div>
                        </div>

                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> 添加 </button>
                <button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" lay-event="delete"> 删除 </button>
            </div>
        </script>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>

    </div>
</div>

<script>
    layui.use(['form', 'table','miniPage','element','upload'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;

        table.render({
            elem: '#currentTableId',
            url: 'order/listAll',
            toolbar: '#toolbarDemo',
            page:true,
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                //icon: 'layui-icon-tips'
            }],

            cols: [[
                {type: "checkbox"},
                {field: 'oid',  title: 'ID',width:75, sort: true},
                {field: 'ordeId',title: '订单编号'},
                { title: '用户',  sort: true, templet:"#luIdTp"},
                { title: '场次',  sort: true,templet:"#changciTp"},
                {field: 'buy_seats',  title: '座位', sort: true},
                {field: 'booking_time',  title: '下单时间', sort: true},
                { title: '电影',  sort: true,templet:"#filmNameTp"},
                { title: '影厅',  sort: true,templet:"#hallNameTp"},
                {field: 'price',  title: '价格', sort: true},
                { title: '状态',  sort: true,templet:"#stateTp"},
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            var result = JSON.stringify(data.field);
            //
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: data.field
            }, 'data');

            return false;
        });

        /**
         * toolbar事件监听
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'add') {   // 监听添加操作
                var content = miniPage.getHrefContent('product/toEdit');
                var openWH = miniPage.getOpenWidthHeight();

                var index = layer.open({
                    title: '添加商品',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            } else if (obj.event === 'delete') {  // 监听删除操作
                var checkStatus = table.checkStatus('currentTableId')
                    , data = checkStatus.data;
                layer.alert(JSON.stringify(data));
            }
        });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            console.info(obj.data)
            if (obj.event === 'edit') {
                var content = miniPage.getHrefContent('product/toEdit');
                var openWH = miniPage.getOpenWidthHeight();
                var index = layer.open({
                    title: '编辑商品',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content
                });
                console.info(obj.data);
                form.val("testForm",obj.data);
                console.info("要选择的类别的id为",obj.data.ptype.tid);
                $("#ptypeSelect").val(obj.data.ptype.tid);
                //更新下拉列表渲染
                form.render("select");
                var imgPath = obj.data.avatar;
                console.info(imgPath)
                //给预览图片的 标签的src属性赋值
                $("#imgPathBro").attr("src","http://localhost:8091/img/"+imgPath);

                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('真的删除行么', function (index) {
                    obj.del();
                    layer.close(index);
                });
            }
        });

    });
</script>