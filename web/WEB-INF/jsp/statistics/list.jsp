<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<style>
    .top-panel {border:1px solid #eceff9;border-radius:5px;text-align:center;}
    .top-panel > .layui-card-body {height:60px;}
    .top-panel-number {line-height:60px;font-size:30px;border-right:1px solid #eceff9;}
    .top-panel-tips {line-height:30px;font-size:12px}
</style>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main welcome">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-xs12 layui-col-md3" style="margin-left: 100px">

                <div class="layui-card top-panel">
                    <div class="layui-card-header">总票数</div>
                    <div class="layui-card-body">
                        <div class="layui-row layui-col-space5">
                            <div id="zquantity" class="layui-col-xs9 layui-col-md9 top-panel-number"style="width: 103%;">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="layui-col-xs12 layui-col-md3" style="margin-left: 200px">

                <div class="layui-card top-panel">
                    <div class="layui-card-header">总收益</div>
                    <div class="layui-card-body">
                        <div class="layui-row layui-col-space5">
                            <div id="zprice" class="layui-col-xs9 layui-col-md9 top-panel-number" style="width: 103%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-row layui-col-space15" style="height:670px">
            <div class="layui-col-xs12 layui-col-md9" >
                <div id="echarts-records" style="background-color:#ffffff;min-height:400px;padding: 10px;height: 550px"></div>
            </div>
            <div class="layui-col-xs12 layui-col-md3">
                <div id="echarts-pies" style="background-color:#ffffff;min-height:400px;padding: 10px"></div>
            </div>
        </div>
        <!--<div class="layui-row layui-col-space15" style="height: 200px">-->
        <!--</div>-->
    </div>
</div>
<script>
    layui.use(['layer','echarts'], function () {
        var $ = layui.jquery,
            layer = layui.layer,
            echarts = layui.echarts;

        var chinese_name="";
        var quantity="";
        var zquantity=0;
        var price="";
        var zprice=0;
        $.post("statistics/listTop10",function (obj) {
            $(obj).each(function () {
               quantity+=this.zquantity+",";
                chinese_name+=this.film.chinese_name+",";
                price+=this.zprice+",";
                zprice+=this.zprice;
                zquantity+=this.zquantity;
            });
            $("#zprice").html("￥"+zprice)
            $("#zquantity").html(zquantity)
            /**
             * 报表功能
             */

            var quantitys =  quantity.split(",");
            var chinese_names=chinese_name.split(",");
            var prices=price.split(",");
            console.info(chinese_names)

            var zPrice=0;
            // for (i=0;i<prices.length;i++){
            //     zPrice += prices[i];
            // }
            console.info(zPrice)
            var echartsRecords = echarts.init(document.getElementById('echarts-records'), 'walden');
            var optionRecords = {
                title: {
                    text: '前十电影票房/收益'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                    }
                },
                legend: {
                    data: ['票房', '收益']
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        boundaryGap: false,
                        data: chinese_names
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [

                    {
                        name: '票房',
                        type: 'line',
                        areaStyle: {},
                        data: quantitys
                    },

                    {
                        name: '收益',
                        type: 'line',
                        stack: '总量',
                        areaStyle: {},
                        data: prices
                    }
                ]
            };
            echartsRecords.setOption(optionRecords);


            /**
             * 玫瑰图表
             */
            var echartsPies = echarts.init(document.getElementById('echarts-pies'), 'walden');
            var optionPies = {
                title: {
                    text: '类型票房-玫瑰图',
                    left: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: ['爱情', '喜剧', '武装', '外国', '于刘洋']
                },
                series: [
                    {
                        name: '访问来源',
                        type: 'pie',
                        radius: '55%',<!--大小-->
                        center: ['50%', '60%'],<!--居中程度-->
                        roseType: 'radius',
                        data: [
                            {value: 335, name: '爱情'},
                            {value: 310, name: '喜剧'},
                            {value: 234, name: '武装'},
                            {value: 135, name: '外国'},
                            {value: 368, name: '于刘洋'}
                        ],
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,<!--选中阴影程度-->
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            echartsPies.setOption(optionPies);


            // echarts 窗口缩放自适应
            window.onresize = function () {
                echartsRecords.resize();
            }

        },"json")

    });
</script>
