<%--
  Created by IntelliJ IDEA.
  User: 郭鹏飞
  Date: 2022/3/13
  Time: 18:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layuimini-main">

    <div class="layui-form layuimini-form">
        <form class="layui-form" lay-filter="testForm">

            <div class="layui-form-item">
                <input id="uid" type="hidden" name="id"/>
                <label class="layui-form-label required">姓名</label>
                <div class="layui-input-block">
                    <input type="text" name="uname" lay-verify="required" lay-reqtext="姓名不能为空" placeholder="请输入姓名"
                           value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">图片上传</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="test1">
                        <i class="layui-icon">&#xe67c;</i>上传图片
                    </button>
                    <img id="imgPathBro" name="imgPath" src="" style="width: 100px;">
                    <input id="Avatar" type="hidden" name="Avatar">


                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">性别</label>
                <div class="layui-input-block">
                    <input type="radio" name="sex" value="男" title="男" checked="">
                    <input type="radio" name="sex" value="女" title="女">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">用户名</label>
                <div class="layui-input-block">
                    <input type="text" name="username" lay-verify="required" lay-reqtext="用户名不能为空" placeholder="请输入用户名"
                           value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">密码</label>
                <div class="layui-input-block">
                    <input type="text" name="password" lay-verify="required" lay-reqtext="密码不能为空" placeholder="请输入密码"
                           value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">手机号</label>
                <div class="layui-input-block">
                    <input type="number" name="phone" lay-verify="required|phone" lay-reqtext="手机不能为空" placeholder="请输入手机"
                           value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">身份证</label>
                <div class="layui-input-block">
                    <input type="text" name="idcard" lay-verify="required|idcard" placeholder="请输入" value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">职业</label>
                <div class="layui-input-block">
                    <select id="selectId" name="rid" lay-verify="required">
                        <option value="">请选择</option>
                        <%--  <option value="1">管理员</option>
                          <option value="1">管理员</option>
                          <option value="1">管理员</option>--%>
                    </select>
                </div>
            </div>
        </form>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
            </div>
        </div>
    </div>
</div>

<script>
    layui.use(['form', 'table', 'upload'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            upload = layui.upload,
            $ = layui.$;


        form.verify({
            idcard:function (value,item) {
                var idcard =/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
                if(!idcard.test(value)){//虽然看起来很可怕 但是亲自验证过的可以用
                    return "请输入正确的身份证号码";
                }
            }
        })




        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();


        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            $.post("user/saveOrUpdate", data.field, function (data) {
                if (data.success) {
                    // 关闭弹出层
                    layer.close(parentIndex);
                    //刷新界面
                    table.reload("currentTableId");
                } else {
                    layer.msg(data.error);
                }
            });
            // 关闭弹出层
            layer.close(index);
            layer.close(parentIndex);
        });

        //绑定图片上传
        var uploadInst = upload.render({
            elem: '#test1' //绑定元素
            , url: 'user/upload' //上传接口
            , done: function (res) {
                //上传完毕回调
                console.info("上传完成", res);
                $("#Avatar").val(res.target);
                //更改预览图片标签的src属性
                $("#imgPathBro").attr("src", "http://localhost:8091/img/" + res.target)
            }
            , error: function () {
                //请求异常回调
            }
        });

        //动态添加下拉框 同时可以设置默认值
        $.ajax({
            url: 'user/listAllRole',
            dataType: 'json',
            type: 'post',
            async: false,
            success: function (data) {
                $.each(data, function (index, item) {
                    console.log("item", item);
                    //option 第一个参数是页面显示的值，第二个参数是传递到后台的值
                    //name="roles['+i+'].id
                    $('#selectId').append(new Option(item.name, item.id));//往下拉菜单里添加元素
                    //设置value（这个值就可以是在更新的时候后台传递到前台的值）为2的值为默认选中
                    //$('#selectId').val(2);
                })
                //form.render(); //更新全部表单内容
                form.render("select"); //刷新表单select选择框渲染

            }
        });
    });
</script>