<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/3/18
  Time: 9:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/html" id="indentityTpl">
    <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> {{d.identity.iname}} </button>
</script>

<script type="text/html" id="imgTpl">
    <img width="50"  src="http://localhost:9998/img/{{d.picture}}">
</script>
<%--<script type="text/html" id="reception1">--%>
<%--<span style="color: red;"> {{d.reception.rname}}</span>--%>
<%--</script>--%>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">

        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">前台用户名称</label>
                            <div class="layui-input-inline">
                                <input type="text" name="keyword" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>

                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>


        <script type="text/html" id="currentTableBar">
            {{#  if(d.id =='6'){ }}
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit"  lay-event="qi" id="qi">启用</a>
            {{#  } }}
            {{#  if(d.id =='7'){ }}
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="jin" id="jin" >禁用</a>
            {{#  } }}
        </script>

    </div>
</div>
<script>
    layui.use(['form', 'table','miniPage','element','upload'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            upload = layui.upload,
            miniPage = layui.miniPage;

        table.render({
            elem: '#currentTableId',
            url: 'leadingUser/listData',
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'lid', width: 110, title: '用户编号', sort: true},
                {field:'luname', width: 150, title: '用户名称',templet:"#reception1"},
                { field: 'password',width: 120, title: '用户密码', sort: true},
                {field: 'picture', width: 180, title: '头像', sort: true,templet:'#imgTpl'},
                {field: 'create_time', width: 150, title: '评价时间', sort: true},
                {field: 'id', width: 150, title: '状态', sort: true,templet:"#indentityTpl"},


            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            page: true,
            skin: 'line'
        });
        //刷新界面
        $("#shuaxin").bind("click",function () {
            window.location.reload();
        });
        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            //执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: data.field
            }, 'data');

            return false;
        });


        table.on('tool(currentTableFilter)', function (obj) {
            var identity;
            var id;
            if (obj.data.identity.id === 6) {
                id = 7;
                identity = "禁用"
            } else if (obj.data.identity.id === 7) {

                id = 6;
                identity = "启用";
            }
            layer.confirm('是否确认' + identity, {icon: 3, title: '提示'}, function (index) {
                //往后台发送ajax请求 保存数据
                $.post("leadingUser/identity", {lid: obj.data.lid, id: id}, function (data) {
                    if (data) {
                        //刷新数据
                        table.reload("currentTableId");
                        layer.close(index);
                        layer.msg(identity + "成功")
                        table.reload("currentTableId")
                    }
                }, "json");
            })
            layer.close();
        });


    });
</script>
