<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2022/3/12
  Time: 22:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layuimini-main">

    <div class="layui-form layuimini-form" lay-filter="userForm">
        <div class="layui-form-item">
            <label class="layui-form-label required">角色名</label>
            <div class="layui-input-block">
                <input id="rid" type="hidden" name="id" />
                <input type="text" name="name" lay-verify="required|name" lay-reqtext="角色名不能为空" placeholder="请输入角色名" value="" class="layui-input">
                <tip>填写角色名称。</tip>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label required">选择模块：</label>
            <div class="layui-input-block">
                <div id="test1"></div>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
            </div>
        </div>
    </div>
</div>
<script>

    layui.use(['form', 'table','tree'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            tree=layui.tree,
            $ = layui.$;




        form.verify({
            name: function(value, item){
                var info;
                var id=$('#rid').val();
                $.ajax({
                    url:"role/checkName",
                    async:false,
                    data:{name:value,id:id},
                    success:function (data) {
                        console.info(data);
                        if (data.success){
                            info="";
                        } else {
                            info="角色名重复";
                        }
                    }
                } );
                return info;
            }
        });

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();

        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            //获取所有选中的节点
            var checkData = tree.getChecked('roleTree');
            console.info(checkData);
            //你可以在浏览器控制台打印一下checkData就知道下面代码为什么要这样写 1,2,3,
            var mids= "";
            $(checkData).each(function () {
                mids+=(this.id+",");
                $(this.children).each(function () {
                    mids+=(this.id+",");
                })
            });
            //如果mids不为空
            if(mids){
                //把mids最后一个多于的逗号去掉
                mids=mids.substring(0,mids.length-1);
            }else{
                layer.msg('请选择角色能操作的菜单');
                return;
            }
            //往后台传递参数
            data.field.mids=mids;
            //往后台发送ajax请求 保存数据
            $.post("role/saveOrUpdate",data.field,function (data) {
                if(data.success){
                    // 关闭弹出层
                    layer.close(parentIndex);
                    //刷新界面
                    table.reload("currentTableId");
                }else{
                    layer.msg(data.error);
                }
            });

            /* var index = layer.alert(JSON.stringify(data.field), {
                 title: '最终的提交信息'
             }, function () {


             });*/


            return false;
        });
        //开启同步
        $.ajaxSettings.async = false;
        $.post("role/listAllMenus",function (data) {
            //渲染
            var inst1 = tree.render({
                elem: '#test1'  //绑定元素
                ,showCheckbox:true
                ,id:"roleTree"
                ,data:data
            });
        });

        //关闭同步
        $.ajaxSettings.async = true;


    });
</script>
