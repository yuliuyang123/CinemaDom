<%--
  Created by IntelliJ IDEA.
  User: 28689
  Date: 2022/3/13
  Time: 17:56
  To change this template use File | Settings | File Templates.
--%>
<script type="text/html" id="ptypeTpl">
    <span>{{d.ptype.tname}}</span>
</script>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layuimini-main">

    <div class="layui-form layuimini-form" lay-filter="productFrom">
        <div class="layui-form-item">
            <label class="layui-form-label required">商品名称</label>
            <div class="layui-input-block">
                <input type="hidden" name="pid" display="nooe">
                <input type="text" name="pname" lay-verify="required" lay-reqtext="商品名不能为空" placeholder="请输入商品名" value="" class="layui-input">

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">商品图片</label>
            <div class="layui-input-block">
                <button type="button" class="layui-btn" id="test1">
                    <i class="layui-icon">&#xe67c;</i>上传图片
                </button>
                <img id="imgPathBro" src="images/3.jpg" style="width: 100px;" >
                <input id="img_product" type="hidden" name="img_product" >


            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label required">商品价格</label>
            <div class="layui-input-block">
                <input type="text" name="price" lay-verify="required" lay-reqtext="商品价格不能为空" placeholder="请输入商品价格" value="" class="layui-input">

            </div>
        </div>
       <%-- <div class="layui-form-item">
            <label class="layui-form-label required">商品类型</label>
            <div class="layui-input-block">
                <input type="text" name="ptype" lay-verify="required" lay-reqtext="商品类型不能为空" placeholder="请输入商品类型" value="" class="layui-input">

            </div>
        </div>--%>

        <div class="layui-form-item">
            <label class="layui-form-label">选择框</label>
            <div class="layui-input-block">
                <select id="ptypeSelect" name="ptype.tid" lay-verify="required">

                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
            </div>
        </div>
    </div>
</div>
<script>
    layui.use(['form', 'table','upload'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            upload = layui.upload,
            $ = layui.$;

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();

        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;


        //绑定图片上传
        var uploadInst = upload.render({
            elem: '#test1' //绑定元素
            ,url: 'product/upload.do' //上传接口
            ,done: function(res){
                //上传完毕回调
                console.info("上传完成",res);
                $("#img_product").val(res.target);
                //更改预览图片标签的src属性
                $("#imgPathBro").attr("src","http://localhost:8081/img/"+res.target)
            }
            ,error: function(){
                //请求异常回调
            }
        });



        //监听提交
        form.on('submit(saveBtn)', function (data) {
            $.post("product/saveOrUpdate.do",data.field,function (data) {

                if (data.success){
                    layer.close(parentIndex);
                    table.reload("currentTableId");

                }else{
                    layer.msg(error);

                }
            })
            // var index = layer.alert(JSON.stringify(data.field), {
            //     title: '最终的提交信息'
            // }, function () {
            //
            //     // 关闭弹出层
            //
            // });


            return false;
        });



        //动态添加下拉框 同时可以设置默认值
        $.ajax({
            url: 'product/listAllPtypes',
            dataType: 'json',
            type:'post',
            async:false,
            success: function (data) {
                $.each(data, function (index, item) {
                    console.log("item",item);
                    //option 第一个参数是页面显示的值，第二个参数是传递到后台的值
                    //name="roles['+i+'].id
                    $('#ptypeSelect').append(new Option (item.tname, item.tid));//往下拉菜单里添加元素
                    //设置value（这个值就可以是在更新的时候后台传递到前台的值）为2的值为默认选中
                    //$('#selectId').val(2);
                })
                //form.render(); //更新全部表单内容
                form.render("select"); //刷新表单select选择框渲染

            }
        });

    });
</script>
