<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: 28689
  Date: 2022/3/10
  Time: 09:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<base href="<%=basePath %>" />
<%--<script src = "lib/jquery-3.4.1/jquery-3.4.1.min.js"></script>--%>
<script type="text/html" id="ptypeTpl">
    <span>{{d.ptype.tname}}</span>
</script>
<html>
<head>
    <title>商品列表界面</title>
    <link rel="stylesheet" href="layui/css/layui.css">

    <div class="layuimini-container layuimini-page-anim">
        <div class="layuimini-main">

            <fieldset class="table-search-fieldset">
                <legend>搜索信息</legend>
                <div style="margin: 10px 10px 10px 10px">
                    <form class="layui-form layui-form-pane" action="">
                        <div class="layui-form-item">
                            <div class="layui-inline">
                                <label class="layui-form-label">商品名称</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="keyword" autocomplete="off" class="layui-input">
                                </div>
                            </div>

                            <div class="layui-inline">
                                <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                            </div>
                        </div>
                    </form>
                </div>
            </fieldset>

            <script type="text/html" id="toolbarDemo">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> 添加 </button>
                    <%--<button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="car"> 购物车 </button>--%>
                    <%--<button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="order"> 订单详情 </button>--%>
                </div>
            </script>


            <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

            <script type="text/html" id="currentTableBar">
                <%--<a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" onclick='addProduct(this)' >添加</a>--%>
                <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
                <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
            </script>

        </div>
    </div>



    <div id="zheZhao" class="layui-form" style="margin: auto;" hidden>

        <table id="carTable" width="70%" class="layui-table" lay-filter="carTableFilter">
            <colgroup>
                <col width="150">
                <col width="150">
                <col width="200">
                <col>
            </colgroup>
            <thead>
            <tr>
                <th>商品编号</th>
                <th>商品名称</th>
                <th>单价</th>
                <th>数量</th>
                <th>小计</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        总计：<span id="zongji"></span>
        <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" onclick="saveCar()" style="float: right"> 保存 </button>

    </div>

</head>
<body>

<script src="lib/jquery-3.4.1/jquery-3.4.1.min.js"></script>
<script type="text/html" id="productTpl">
    <img width="50" height="40" id="imgPathPro"  src="http://localhost:8081/img/{{d.img_product}}">  {{d.pname}}

</script>

<script>

    layui.use(['form', 'table','miniPage','element'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;

        table.render({
            elem: '#currentTableId',
            url: 'product/listData.do',
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'pid', width: 200, title: 'ID', sort: true},
                // {field: 'img_product', width: 100, title: '图片路径', sort: true,opacity:0},
                {field: 'pname', width: 200, title: '商品名称',templet:"#productTpl"},
                {field: 'price', width: 200, title: '商品价格', sort: true},
                {field: 'ptype',  title: '商品类型',templet: "#ptypeTpl"},
                {title: '操作', minWidth: 250, toolbar: '#currentTableBar', align: "center"}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            page: true,
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            var result = JSON.stringify(data.field);


            //执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: data.field
            }, 'data');

            return false;
        });

        /**
         * toolbar事件监听
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'add') {   // 监听添加操作
                var content = miniPage.getHrefContent('product/toEdit');
                var openWH = miniPage.getOpenWidthHeight();

                var index = layer.open({
                    title: '添加商品',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['600px',  '400px'],
                    content: content,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            }

        });


        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {
                var content = miniPage.getHrefContent('product/toEdit');
                var openWH = miniPage.getOpenWidthHeight();

                var index = layer.open({
                    title: '编辑商品',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['600px',  '400px'],
                    content: content,
                });
                form.val("productFrom",data)

                //获取到当前商品的图片路径
                var imgPath = obj.data.img_product;
                //给预览图片的 标签的src属性赋值
                $("#imgPathBro").attr("src","http://localhost:8081/img/"+imgPath);
                console.info(obj.data)

                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('真的删除行么', function (index) {
                    // obj.del();
                    $.post("product/delete",{pid:obj.data.pid},function (data) {
                        console.info(data);
                        if (data.success) {
                            layer.close(index);
                            //刷新数据
                            table.reload('currentTableId');
                        } else {
                            layer.msg(data.error);
                        }
                    })
                    // alert(index)
                    layer.close(index);
                });
            }
        });

    });
</script>


</body>
</html>
