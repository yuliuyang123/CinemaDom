<%--
  Created by IntelliJ IDEA.
  User: DEll
  Date: 2022/3/15
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form id="mForm" class="layui-form" action="" lay-filter="testForm">
    <div class="layui-form-item">
        <label class="layui-form-label">商品类型名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="tid" id="tid" />
            <input type="text" name="tname" required  lay-verify="required|tname" placeholder="请输入电影类型名称" autocomplete="off" class="layui-input">
        </div>
    </div>




    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    layui.use(['form', 'table', 'upload'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            upload = layui.upload,
            $ = layui.$;





        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();


        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;

        //监听提交
        form.on('submit(formDemo)', function (data) {
            console.info(data);
            // layer.msg(JSON.stringify(data.field));
            //往后台发送ajax请求 保存商品
            $.post("ptype/saveOrUpdate", data.field, function (data) {
                console.info(data);
                //如果操作成功
                if (data.success) {
                    // 关闭弹出层
                    layer.close(parentIndex);
                    //刷新界面
                    table.reload("currentTableId");
                } else {
                    layer.msg(data.error);
                }
            })
            return false;
        });


    });
</script>