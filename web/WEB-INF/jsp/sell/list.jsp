<%--
  Created by IntelliJ IDEA.
  User: 郭鹏飞
  Date: 2022/3/21
  Time: 0:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="layuimini-container layuimini-page-anim" style=" background-color: white;margin-top:10px;height: 800px; margin-left: 10px; width: 1563px;">
    <span class="panel-title hot-title" style="margin-top: 20px"></span>
    <div class="panel-content" style="overflow:auto; margin-left: 10px">
        <ul class="movie-list movie-hot">
        </ul>
    </div>
</div>
</body>
<script>
    layui.use(['form', 'table', 'miniPage', 'element', 'tree'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            tree = layui.tree,
            miniPage = layui.miniPage;
        $(function () {
            initHostMovie()
        })

        function initHostMovie() {
            var MoiveLiHot = $(".movie-hot");
            var htmlHot, htmlOn;
            var ListLength;
            var HotNum = $(".hot-title");

            $.ajax({
                type: 'post',
                url: "sell/listAll",
                dataType: 'json',
                data: {},
                success: function (obj) {
                    console.log(obj);
                    HotNum.append("<span class=\"textcolor_red\">正在热映（" + obj.length + "）</span>");

                    for (var i = 0; i < obj.length; i++) {
                        htmlHot =
                            "<li>" +
                            "<div class=\"movie-item\">" +
                            "<a   target=\"_blank\" data-act=\"playingMovie-click\" data-val=\"" + obj[i].film.id + "\">" +
                            "<div class=\"movie-poster\" style=\"cursor:default;\">" +
                            "<img id=\"moive_picture\" src= \"http://localhost:8091/img/" + obj[i].film.imgUrl + "\">" + "<div class=\"movie-overlay movie-overlay-bg\"><div class=\"movie-info\">" +
                            "<div class=\"movie-score\"><i id=\"moive_score\" class=\"integer\"> 10 </i></div>" +
                            "<div class=\"movie-title movie-title-padding\" title=\"" + obj[i].film.chinese_name + "\">" + obj[i].film.chinese_name + "</div>" +
                            "</div>" +
                            "</div>" +
                            "</div>" +
                            "</a>" +
                            "<div class=\"movie-detail movie-detail-strong movie-sale\">" +
                            // "<a id=\"goupiao" + i + "\" href=\"sell/toBuyTickets?fid=\" + obj[i].film.id + \"\"  class=\"active\" target=\"_blank\" data-act=\"salePlayingMovie-click\" data-val=\"{movieid:42964}\">购 票</a>" +
                            "<a id='goupiao' value='"+obj[i].film.id+"'class=\"active\" target=\"_blank\" data-act=\"salePlayingMovie-click\" data-val=\"{movieid:42964}\">购 票</a>" +
                            "</div>" +
                            "</div>" +
                            "</li>";
                        MoiveLiHot.append(htmlHot);
                    }
                }
            });
        }

        $(document).off("click","#goupiao").on('click', "#goupiao",function () {
            var fid = $(this).attr('value');
            console.info(fid);
            var content = miniPage.getHrefContent("sell/toBuyTickets?fid="+fid+"");
            var openWH = miniPage.getOpenWidthHeight();
            var index = layer.open({
                title:'场次信息',
                type: 1,
                shade: 0.2,
                maxmin:true,
                shadeClose: true,
                area: [openWH[0] + 'px', openWH[1] + 'px'],
                offset: [openWH[2] + 'px', openWH[3] + 'px'],
                content: content,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        })
    });
</script>
</html>
