<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2022/3/12
  Time: 22:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/buySeat.css" media="all">
<link type="text/css" href="${pageContext.request.contextPath}/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="${pageContext.request.contextPath}/static/bootstrap/css/bootstrap.css" rel="stylesheet">
<%--<style>--%>
<%--.poster{--%>
<%--width: 115px;--%>
<%--height: 158px;--%>
<%--border: 2px solid #fff;--%>
<%---webkit-box-shadow: 0 2px 7px 0 hsl(0deg 0% 53% / 50%);--%>
<%--box-shadow: 0 2px 7px 0 hsl(0deg 0% 53% / 50%);--%>
<%--float: left--%>
<%--}--%>
<%--.poster>img{--%>
<%--width: 100%;--%>
<%--height: 100%;--%>
<%--}--%>
<%--.movie-info{--%>
<%--width: 328px;--%>
<%--height: 160px;--%>
<%--}--%>
<%--.content{--%>

<%--}--%>

<%--</style>--%>
<div class="layuimini-main" style=" height: 700px; ">
    <div class="main" class="layui-form layuimini-form" lay-filter="userForm"
         style="   height: 700px  ;width:1270px;float: left; margin-right: 30px">
        <div style="  float: left;margin-left: 100px; width: 650px; margin-top: 30px">
            <!-- 主页 -->
            <div class="hall" style="height: 650px">
                <div class="seat-example">
                    <div class="selectable-example example">
                        <span>可选座位</span>
                    </div>
                    <div class="sold-example example">
                        <span>已售座位</span>
                    </div>
                    <div class="selected-example example">
                        <span>已选座位</span>
                    </div>
                </div>
                <div class="seats-block">
                    <div class="row-id-container">
                    </div>
                    <div class="seats-container">
                        <div class="screen-container">
                            <div class="screen">银幕中央</div>
                        </div>
                        <div class="seats-wrapper">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 侧页 -->
        <div class="side" style="margin-top: 30px;margin-left: 50px">
            <!-- 电影 -->
            <div class="movie-info clearfix">
            </div>
            <!-- 场次 -->
            <div class="show-info">

            </div>
            <div class="ticket-info">
                <div class="no-ticket" style="display: block;">
                    <p class="buy-limit">座位：后台一次可选无限个座位</p>
                </div>
                <div class="has-ticket" style="display: none; width: 333px">
                    <span class="text">座位：</span>
                    <div class="ticket-container" style="overflow:auto; width: 230px;height: 70px;">
                    </div>
                </div>
                <div class="total-price">
                    <span>总价：</span>
                    <span class="price"></span>
                </div>
            </div>
            <div class="confirm-order">
                <div class="email">
                    <span>当前值班人员：${sessionScope.SESSION_USER.uname}</span>
                </div>
                <div class="confirm-btn disable" id="Confirm">确认选座</div>
            </div>
        </div>
    </div>
</div>
<script>
    layui.use(['form', 'table', 'tree'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            tree = layui.tree,
            $ = layui.$;
        //选中场次id
        var tid = ${param.tid};
        //电影id
        var fid = ${param.fid};
        var luid = "${sessionScope.SESSION_USER.uid}";
        var SeatMax = 0;
        var PriceTemp = 1;
        var price;
        var buySeats = "";
        $(".seats-wrapper").on('click', '.seat', function () {
            var row = $(this).attr('row');
            var col = $(this).attr('col');
            var Ticket = $(".ticket-container");
            var NoTicket = $(".no-ticket")[0];
            var HasTicket = $(".has-ticket")[0];
            var ConfirmBtn = $(".confirm-btn")[0];
            var TicketPrice = $(".price");
            NoTicket.style.display = "none";
            HasTicket.style.display = "block";
            // alert(SeatMax,"max")
            //
            var rclass = $(this).attr('class');
            // if((SeatMax>11)&&(rclass == "seat")){
            //       layer.msg("后台一次只能选12个坐")
            //       return;
            //   }else {
            if ($(this).is('.selectable')) {
                $(this).removeClass('selectable');
                SeatMax--;
                $("[data-index=" + row + col + "]").remove();
                price = PriceTemp * SeatMax
                TicketPrice.text("￥" + price);
            } else {
                $(this).addClass('selectable');
                SeatMax++;
                Ticket.append("<span class=\"ticket\" data-index=\"" + row + col + "\">" + row + col + "</span>");
                price = PriceTemp * SeatMax
                TicketPrice.text("￥" + price);
            }
            if (SeatMax == 0) {
                NoTicket.style.display = "block";
                HasTicket.style.display = "none";
                ConfirmBtn.className = "confirm-btn disable";
            } else {
                NoTicket.style.display = "none";
                HasTicket.style.display = "block";
                ConfirmBtn.className = "confirm-btn";
            }
            // }
        });

        $(function () {
            initInformation()
        })

        function initSeat(hang, lie) {
            var flag;
            var $rowIdContainer = $(".row-id-container");
            for (var i = 1; i <= hang; i++) {
                $('<span class="row-id">').text(i).appendTo($rowIdContainer);
                var $seat = $(".seats-wrapper");
                var $row = $('<div class="row">').appendTo($seat);
                for (var j = 1; j <= lie; j++) {
                    if (buySeats.length == 0) {
                        var $col = $('<span class="seat">').appendTo($row);
                        $col.attr('row', i + "排").attr('col', j + "座")
                    } else {
                        flag = 0;
                        for (var p = 0; p < buySeats.length; p++) {
                            if (buySeats[p] == (i + "排" + j + "座")) {
                                flag = 1;
                            }
                        }
                        if (flag == 1) {
                            var $col = $('<span class="seat sold">').appendTo($row);
                            $col.attr('row', i + "排").attr('col', j + "座")
                        } else {
                            var $col = $('<span class="seat">').appendTo($row);
                            $col.attr('row', i + "排").attr('col', j + "座")
                        }
                    }
                }
            }
        }

        //点击具体座位事件

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();

        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;


        //确定选座
        $(document).on('click', '#Confirm', function () {
            //获取选中的座位
            var srt = ""
            //座位数
            var seat = ""
            var numseat = 0;
            $('span[class="ticket"]').each(function () {
                srt = $(this).attr('data-index') + ",";
                seat += srt;
                numseat++;
            });
            $.post("order/save", {
                tid: tid,
                buy_seats: seat,
                price: price,
                luid: luid,
                numseat: numseat,
                fid:fid
            }, function (data) {
                if (data.success) {
                    // 关闭弹出层
                    layer.closeAll();
                    layer.msg("下单成功")
                    //刷新界面
                    table.reload("currentTableId");
                } else {
                    layer.msg(data.error);
                }
            }, "json")
        });

        function initInformation() {
            var movieInfo = $(".movie-info.clearfix");
            var showInfo = $(".show-info");
            $.ajax({
                type: 'post',
                url: "sell/findTicketSalesByTId?tid=" + tid + "",
                dataType: 'json',
                success: function (obj) {
                    var TempLength = obj.orderList.length;
                    var buySeat = "";
                    for (var i = 0; i < TempLength; i++) {
                        buySeat += obj.orderList[i].buy_seats
                    }
                    buySeats = buySeat.split(",")
                    PriceTemp = obj.price;
                    movieInfo.append(
                        "<div class='poster'>" +
                        "<img src= \"http://localhost:8091/img/" + obj.film.imgUrl + "\">" +
                        "</div>" +
                        "<div class=\"content\">" +
                        "<p class=\"name text-ellipsis\">" + obj.film.chinese_name + "</p>" +
                        "<div class=\"info-item\">" +
                        "<span>类型：</span>" +
                        "<span class=\"value\">" + obj.film.ftype.name + "</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>时长：</span>" +
                        "<span class=\"value\">\"" + obj.film.duration + "分钟\"</span>" +
                        "</div>" +
                        "</div>"
                    );
                    showInfo.append(
                        "<div class=\"info-item\">" +
                        "<span>影院：</span>" +
                        "<span class=\"value\">横店影城</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>影厅：</span>" +
                        "<span class=\"value\">" + obj.hall.hname + "</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>版本：</span>" +
                        "<span class=\"value\">" + obj.film.country + "</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>场次：</span>" +
                        "<span class=\"value\">" + obj.opening_time + "</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>票价：</span>" +
                        "<span class=\"value\">￥" + obj.price + "/张</span>" +
                        "</div>"
                    );
                    initSeat(obj.hall.hang, obj.hall.lie);
                }
            });
        }

    });
</script>
