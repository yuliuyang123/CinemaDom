<%--
  Created by IntelliJ IDEA.
  User: 郭鹏飞
  Date: 2022/3/13
  Time: 2:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/html" id="FilmTpl">
    <span > {{d.film.chinese_name}}</span>
</script>
<script type="text/html" id="HallTpl">
    <span > {{d.hall.hname}}</span>
</script>
<script type="text/html" id="imgTpl">
    <img width="50"  src="http://localhost:8081/img/{{d.imgUrl}}">
</script>
<style type="text/css">

    .layui-table-cell {
        height: auto;
        line-height: 60px;
    }
</style>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">购票</a>
        </script>

    </div>
</div>

<script>
    layui.use(['form', 'table','miniPage','element','upload'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;
        var fid = ${param.fid};
        table.render({
            elem: '#currentTableId',
            url: "sell/listData?fid="+fid+"",
            toolbar: '#toolbarDemo',
            page:true,
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'tid', width: 80, title: 'ID', sort: true},
                {title: '电影名',sort: true,templet:"#FilmTpl"},//外键
                {title: '影厅',templet:"#HallTpl"},//外键
                {field: 'opening_time', title: '放映时间'},
                {field: 'break_time', title: '结束时间'},
                {field: 'price',  title: '价格', sort: true},
                {field: 'creation_time',  title: '创建时间', sort: true},
                {field: 'remaining_votes',  title: '剩余票数', sort: true},
                {title: '操作', minWidth: 100, toolbar: '#currentTableBar', align: "center"}

            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            skin: 'line'
        });




        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            var tid =obj.data.tid;
            if (obj.event === 'edit') {
                var content = miniPage.getHrefContent("sell/buyTickets?tid="+tid+"&fid="+fid+"");
                var openWH = miniPage.getOpenWidthHeight();
                var index = layer.open({
                    title: '购票',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ["1300px","770px"],
                    offset: ["70px"],
                    content: content
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });

                return false;
            }
        });

    });
</script>