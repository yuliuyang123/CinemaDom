<%--
  Created by IntelliJ IDEA.
  User: DEll
  Date: 2022/3/15
  Time: 10:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form id="mForm" class="layui-form" action="" lay-filter="testForm">
    <div class="layui-form-item">
        <label class="layui-form-label">电影中文名称</label>
        <div class="layui-input-block">
            <input id="uid" type="hidden" name="id" />
            <input type="text" name="chinese_name" required  lay-verify="required|chinese_name" placeholder="请输入电影中文名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">电影英文名称</label>
        <div class="layui-input-block">
            <input type="text" name="english_name" required  lay-verify="required" placeholder="请输入电影英文名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">电影海报</label>
        <div class="layui-input-block">
            <button type="button" class="layui-btn" id="test1">
                <i class="layui-icon">&#xe67c;</i>上传图片
            </button>
            <img id="imgUrlBro" name="imgPath" src="" style="width: 100px;" >
            <input id="imgUrl" type="hidden" name="imgUrl" lay-verify="required" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">上映时间</label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" id="showtimes" placeholder="yyyy-MM-dd" autocomplete="off" name="showtimes" lay-verify="required">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">电影播放时长</label>
        <div class="layui-input-block">
            <input type="text" name="duration" required  lay-verify="required" placeholder="请输入电影播放时长" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">电影简介</label>
        <div class="layui-input-block">
            <input type="text" name="introduction" required  lay-verify="required" placeholder="请输入电影简介" autocomplete="off" class="layui-input">
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">拍摄国家</label>
        <div class="layui-input-block">
            <input type="text" name="country" required  lay-verify="required" placeholder="请输入电影拍摄国家" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">导演</label>
        <div class="layui-input-block">
            <input type="text" name="director" required  lay-verify="required" placeholder="请输入电影拍摄导演" autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">导演图片</label>
        <div class="layui-input-block">
            <button type="button" class="layui-btn" id="test2">
                <i class="layui-icon">&#xe67c;</i>上传图片
            </button>
            <img id="imgUrlBro2" src="imgs/微信图片_20210613151745.jpg" style="width: 100px;" >
            <input id="imgUrl2" type="hidden" name="dmg" lay-verify="required"
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">演员</label>
        <div class="layui-input-block">
            <input type="text" name="actors" required  lay-verify="required" placeholder="请输入电影拍摄演员" autocomplete="off" class="layui-input">
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">演员图片</label>
        <div class="layui-input-block">
            <button type="button" class="layui-btn" id="test3">
                <i class="layui-icon">&#xe67c;</i>上传图片
            </button>
            <img id="imgUrlBro3" src="imgs/微信图片_20210613151745.jpg" style="width: 100px;" >
            <input id="imgUrl3" type="hidden" name="amg" lay-verify="required" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">选择框</label>
        <div class="layui-input-block">
            <select id="ftypeSelect" name="ftype.tid" lay-verify="required">

            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    layui.use(['form', 'table', 'upload','laydate'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            upload = layui.upload,
            laydate = layui.laydate,
            $ = layui.$;

        form.verify({
            chinese_name: function(value, item){
                var info;
                var id=$('#uid').val();
                $.ajax({
                    url:"film/checkName",
                    async:false,
                    data:{name:value,id:id},
                    success:function (data) {
                        console.info(data);
                        if (data.success){
                            info="";
                        } else {
                            info="电影名重复";
                        }
                    }
                } );
                return info;
            }
        });



        laydate.render({
            elem: '#showtimes',
            format:'yyyy年MM月dd日',
            trigger:'click'
        });

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();


        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;

        //监听提交
        form.on('submit(formDemo)', function(data){
            console.info(data);
            // layer.msg(JSON.stringify(data.field));
            //往后台发送ajax请求 保存商品
            $.post("film/saveOrUpdate",data.field,function (data) {
                console.info(data);
                //如果操作成功
                if(data.success){
                    // 关闭弹出层
                    layer.close(parentIndex);
                    //刷新界面
                    table.reload("currentTableId");
                }else{
                    layer.msg(data.error);
                }
            })
            return false;
        });

        //绑定图片上传
        var uploadInst = upload.render({
            elem: '#test1' //绑定元素
            , url: 'film/upload' //上传接口
            , done: function (res) {
                //上传完毕回调
                console.info("上传完成", res);
                $("#imgUrl").val(res.target);
                //更改预览图片标签的src属性
                $("#imgUrlBro").attr("src", "http://localhost:8091/img/" + res.target)
            }
            , error: function () {
                //请求异常回调
            }
        });


        //绑定图片上传
        var uploadInst = upload.render({
            elem: '#test2' //绑定元素
            , url: 'film/upload' //上传接口
            , done: function (res) {
                //上传完毕回调
                console.info("上传完成", res);
                $("#imgUrl2").val(res.target);
                //更改预览图片标签的src属性
                $("#imgUrlBro2").attr("src", "http://localhost:9998/img/" + res.target)
            }
            , error: function () {
                //请求异常回调
            }
        });



        //绑定图片上传
        var uploadInst = upload.render({
            elem: '#test3' //绑定元素
            , url: 'film/upload' //上传接口
            , done: function (res) {
                //上传完毕回调
                console.info("上传完成", res);
                $("#imgUrl3").val(res.target);
                //更改预览图片标签的src属性
                $("#imgUrlBro3").attr("src", "http://localhost:9998/img/" + res.target)
            }
            , error: function () {
                //请求异常回调
            }
        });


        //动态添加下拉框 同时可以设置默认值
        $.ajax({
            url: 'film/listAllFtypes',
            dataType: 'json',
            type:'post',
            async:false,
            success: function (data) {
                console.info(data)
                $.each(data, function (index, item) {
                    console.log("item",item);
                    //option 第一个参数是页面显示的值，第二个参数是传递到后台的值
                    //name="roles['+i+'].id
                    $('#ftypeSelect').append(new Option (item.name, item.tid));//往下拉菜单里添加元素
                    //设置value（这个值就可以是在更新的时候后台传递到前台的值）为2的值为默认选中
                    //$('#selectId').val(2);
                })
                //form.render(); //更新全部表单内容
                form.render("select"); //刷新表单select选择框渲染

            }
        });
    });
</script>