<%--
  Created by IntelliJ IDEA.
  User: DEll
  Date: 2022/3/13
  Time: 2:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/html" id="ftypeTpl">
    <span style="color: red;"> {{d.ftype.name}}</span>
</script>
<script type="text/html" id="imgTpl">
    <img width="50"  src="http://localhost:8091/img//{{d.imgUrl}}">
</script>
<script type="text/html" id="dmg">
    <img width="50"  src="http://localhost:8091/img/{{d.dmg}}">
</script>
<script type="text/html" id="amg">
    <img width="50"  src="http://localhost:8091/img/{{d.amg}}">
</script>
<style type="text/css">

    .layui-table-cell {
        height: auto;
        line-height: 60px;
    }
</style>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">
        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">电影名</label>
                            <div class="layui-input-inline">
                                <input type="text" name="keyword" autocomplete="off" class="layui-input">
                            </div>
                        </div>


                        <div class="layui-inline">
                            <label class="layui-form-label">演员</label>
                            <div class="layui-input-inline">
                                <input type="text" name="actors" autocomplete="off" class="layui-input">
                            </div>
                        </div>


                        <div class="layui-inline">
                            <label class="layui-form-label">导演</label>
                            <div class="layui-input-inline">
                                <input type="text" name="director" autocomplete="off" class="layui-input">
                            </div>
                        </div>


                        <div class="layui-inline">
                            <label class="layui-form-label">国家</label>
                            <div class="layui-input-inline">
                                <input type="text" name="country" autocomplete="off" class="layui-input">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">电影类型</label>
                            <div class="layui-input-block"  style="width: 190px;">
                                <select name="tid" id="ftypeSelect1" >
                                    <%--{{d.ftype.name}}--%>
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>

                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> 添加 </button>
            </div>
        </script>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a>
        </script>

    </div>
</div>

<script>
    layui.use(['form', 'table','miniPage','element','upload'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            miniPage = layui.miniPage;


        $(function () {
            $.ajax({
                url: 'film/listAllFtypes',
                dataType: 'json',
                type:'post',
                async:false,
                success: function (data) {
                    //console.info("类型：" + data);
                    $.each(data, function (index, item) {
                        // console.log("item",item);
                        //option 第一个参数是页面显示的值，第二个参数是传递到后台的值
                        $('#ftypeSelect1').append(new Option(item.name, item.tid));//往下拉菜单里添加元素
                        //设置value（这个值就可以是在更新的时候后台传递到前台的值）为2的值为默认选中
                        //$('#selectId').val(2);
                    })
                    //form.render(); //更新全部表单内容
                    form.render("select"); //刷新表单select选择框渲染
                }
            });
        })


        table.render({
            elem: '#currentTableId',
            url: 'film/listData',
            toolbar: '#toolbarDemo',
            page:true,
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                //icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 80, title: 'ID', sort: true},
                {field: 'chinese_name', title: '中文名称',templet:"#movieTpl"}
                ,{field: 'english_name', title: '英文名称', width:100}
                ,{field: 'imgUrl', title: '电影海报', width:100,templet:"#imgTpl"}
                ,{field: 'duration', title: '播放时长', width:100,sort:true}
                ,{field: 'showtimes', title: '上映时间', width:100, sort: true}
                ,{field: 'introduction', title: '电影简介', width:100}
                ,{field: 'country', title: '拍摄国家', width:100}
                ,{field: 'director', title: '拍摄导演', width:100}
                ,{field: 'dmg', title: '导演图片', width:100,templet:"#dmg"}
                ,{field: 'actors', title: '参演演员', width:100}
                ,{field: 'amg', title: '演员图片', width:100,templet:"#amg"}
                ,{ title: '类别名称', width:100, sort: true,templet:"#ftypeTpl"}
                ,{title: '操作', minWidth: 150, toolbar: '#currentTableBar', align: "center"}

            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            //执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: data.field
            }, 'data');

            return false;
        });

        /**
         * toolbar事件监听
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'add') {   // 监听添加操作
                var content = miniPage.getHrefContent('film/toEdit');
                var openWH = miniPage.getOpenWidthHeight();

                var index = layer.open({
                    title: '添加电影',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            } else if (obj.event === 'delete') {  // 监听删除操作
                var checkStatus = table.checkStatus('currentTableId')
                    , data = checkStatus.data;
                layer.alert(JSON.stringify(data));
            }
        });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj)
        });

        table.on('tool(currentTableFilter)', function (obj) {
            console.info(obj.data)
            if (obj.event === 'edit') {
                var content = miniPage.getHrefContent('film/toEdit');
                var openWH = miniPage.getOpenWidthHeight();
                var index = layer.open({
                    title: '编辑电影',
                    type: 1,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content
                });
                //回显数据
                form.val("testForm",obj.data);
                console.info("要选择的类别的id为",obj.data.ftype.tid);
                //让类别下拉列表选中 实际值为obj.data.mtype.tid的
                $("#ftypeSelect").val(obj.data.ftype.tid);
                //更新下拉列表渲染
                form.render('select');
                //获取到当前商品的图片路径
                var imgUrl = obj.data.imgUrl;
                //给预览图片的 标签的src属性赋值
                $("#imgUrlBro").attr("src","http://localhost:8091/img/"+imgUrl);

                var dmg = obj.data.dmg;
                //给预览图片的 标签的src属性赋值
                $("#imgUrlBro2").attr("src","http://localhost:9998/img/"+dmg);

                var amg = obj.data.amg;
                //给预览图片的 标签的src属性赋值
                $("#imgUrlBro3").attr("src","http://localhost:9998/img/"+amg);

                return false;
            } else if (obj.event === 'delete') {
                layer.confirm('真的删除电影么', function (index) {
                    // alert(obj.data.tid);
                    // alert("1")
                    $.get("film/delete",{id:obj.data.id},function (data) {
                        if (data.success){

                        } else {
                            layer.msg(data.error);
                        }
                    })
                    obj.del();
                    layer.close(index);
                });
            }
        });

    });
</script>