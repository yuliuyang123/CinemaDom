package com.aaa.entity;

public class Comments {
    private Long id;
    private String content;
    private String create_time;
    public Film film;
    private LeadingUser leadingUser;

    public LeadingUser getLeadingUser() {
        return leadingUser;
    }

    public void setLeadingUser(LeadingUser leadingUser) {
        this.leadingUser = leadingUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }
}
