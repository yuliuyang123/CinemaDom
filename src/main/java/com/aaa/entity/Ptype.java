package com.aaa.entity;

/**
 * 商品类别管理实体类
 */
public class Ptype {

    private Long tid;
    private  String tname;

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }
}
