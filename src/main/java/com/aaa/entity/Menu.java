package com.aaa.entity;

import java.util.List;

public class Menu {
    private Long id;
    private String name;
    private String path;
    private Integer lev;

    //命名空间 权限标识
    private String namespace;

    //当前菜单的子菜单
    private List<Menu> children;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getLev() {
        return lev;
    }

    public void setLev(Integer lev) {
        this.lev = lev;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public List<Menu> getChildren() {
        return children;
    }

    public void setChildren(List<Menu> children) {
        this.children = children;
    }
    //菜单标题
    public String getTitle(){
        return this.name;
    }

    //让菜单默认展开
    public Boolean getSpread(){
        return true;
    }
}
