package com.aaa.entity;

public class Product {
    private Long pid;
    private String pname;
    private Double price;
    private Ptype ptype;
    private String img_product;
    private String create_date;

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public Ptype getPtype() {
        return ptype;
    }

    public void setPtype(Ptype ptype) {
        this.ptype = ptype;
    }

    public String getImg_product() {
        return img_product;
    }

    public void setImg_product(String img_product) {
        this.img_product = img_product;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
