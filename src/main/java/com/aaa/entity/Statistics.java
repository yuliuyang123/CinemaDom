package com.aaa.entity;

public class Statistics {
    private Long sid;
    private Long zquantity;
    private Double zprice;
    private Film film;

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public Long getZquantity() {
        return zquantity;
    }

    public void setZquantity(Long zquantity) {
        this.zquantity = zquantity;
    }

    public Double getZprice() {
        return zprice;
    }

    public void setZprice(Double zprice) {
        this.zprice = zprice;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }
}
