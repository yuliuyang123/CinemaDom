package com.aaa.interceptor;

import com.aaa.entity.User;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor implements HandlerInterceptor {
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取session
        HttpSession session = request.getSession();

        //获取session中的用户对象
        User user = (User)session.getAttribute("SESSION_USER");
        //判断user 是否为null
        if(user==null){
            response.sendRedirect("login");
            return false;
        }else {

            return true;
        }

    }
}
