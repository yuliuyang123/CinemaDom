package com.aaa.util;

import org.springframework.stereotype.Component;

@Component
public class Upload {
        private Integer code= 0;
        private String message="成功";
        private String data ="";
        private String src="";

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
