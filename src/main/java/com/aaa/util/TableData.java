package com.aaa.util;

import java.util.List;

public class TableData<T> {
    //请求是否成功 0成功
    private Integer code=0;
    //错误信息
    private String msg="";
    //总条数
    private Integer count=0;
    //每页的数据
    private List<T> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
