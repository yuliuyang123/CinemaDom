package com.aaa.controller;

import com.aaa.entity.Menu;
import com.aaa.entity.Role;
import com.aaa.service.MenuService;
import com.aaa.service.RoleService;
import com.aaa.service.UserService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 角色管理控制器
 */
@RequestMapping("/role")
@Controller
public class RoleController {


    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuService menuService;
    /**
     * 跳转到角色列表界面
     * @return
     */
    @RequestMapping("/toList")
    public String toList(){

        return "role/list";
    }


    /**
     * 获取所有的角色数据
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public TableData<Role> listData(@RequestParam(defaultValue = "") String keyword, Integer page, Integer limit){
        TableData<Role> tableData = roleService.page(keyword,page, limit);
        return  tableData;
    }

    /**
     * 跳转到角色编辑界面
     * @return
     */
    @RequestMapping("/toEdit")
    public String toEdit(){

        return "role/edit";
    }

    /**
     * 保存或者修改角色的信息
     * @param role
     * @param  mids 角色能操作的模块 格式如2,3,4
     * @return
     */
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public DefaultMsg  saveOrUpdate(Role role,String mids){
        DefaultMsg defaultMsg = roleService.saveOrUpdate(role,mids);
        return defaultMsg;
    }


    /**
     * 查询所有的菜单
     * @return
     */
    @RequestMapping("/listAllMenus")
    @ResponseBody
    public List<Menu> listAllMenus(){
        List<Menu> menus = menuService.listAllByLevel();
        return  menus;
    }

    /**
     * 查询角色能操作的模块
     * @param id 角色id
     * @return
     */
    @RequestMapping("/listRoleMenus")
    @ResponseBody
    public List<Menu> listRoleMenus(Long id){
        List<Menu> menus = menuService.listRoleMenus(id);
        return  menus;
    }



    @RequestMapping("/checkName")
    @ResponseBody
    public DefaultMsg checkName(String name,Integer id){
        DefaultMsg defaultMsg = roleService.checkName(name,id);
        return defaultMsg;
    }

}
