package com.aaa.controller;

import com.aaa.entity.Film;
import com.aaa.entity.Ftype;
import com.aaa.service.FilmService;
import com.aaa.service.FtypeService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.DefaultMsgP;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/film")
public class FilmController {
    @Autowired
    private FilmService filmService;
    @Autowired
    private FtypeService ftypeService;

    @RequestMapping("/toList")
    public String toList(){
        return "film/list";
    }

    @RequestMapping("/listData")
    @ResponseBody
    public TableData<Film> listData(@RequestParam(defaultValue = "")String keyword,String country,String director,String actors,String tid,Integer page,Integer limit){
        TableData<Film> tableData = filmService.page(keyword,country,director,actors,tid, page, limit);
        return tableData;
    }


    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public DefaultMsg saveOrUpdate(Film film){
        DefaultMsg defaultMsg=filmService.saveOrUpdate(film);
        return defaultMsg;
    }

    @RequestMapping("/delete")
    @ResponseBody
    public DefaultMsg delete(Long id){
        DefaultMsg defaultMsg=filmService.delete(id);
        return defaultMsg;
    }

    @RequestMapping("/listAllFtypes")
    @ResponseBody
    public List<Ftype> listAllFtypes(){
        List<Ftype> ftypes = ftypeService.listAll();
        return ftypes;
    }

    @RequestMapping("/upload")
    @ResponseBody
    public DefaultMsgP upload(MultipartFile file){
        String saneName= UUID.randomUUID().toString();
        File saveFile = new File("E:/images/" + saneName);
        DefaultMsgP defaultMsg=new DefaultMsgP();
        try {
            file.transferTo(saveFile);
            defaultMsg.setTarget(saneName);
        } catch (IOException e) {
            defaultMsg.setError("上传文件失败");
            e.printStackTrace();
        }
        return defaultMsg;
    }

    @RequestMapping("/toEdit")
    public String toEdit(){
        return "film/edit";
    }



    @RequestMapping("/checkName")
    @ResponseBody
    public DefaultMsg checkName(String name,Integer id){
        DefaultMsg defaultMsg = filmService.checkName(name,id);
        return defaultMsg;
    }
}
