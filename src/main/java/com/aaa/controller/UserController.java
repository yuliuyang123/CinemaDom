package com.aaa.controller;

import com.aaa.entity.Role;
import com.aaa.entity.User;
import com.aaa.entity.UserRoleVO;
import com.aaa.service.RoleService;
import com.aaa.service.UserService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.DefaultMsgP;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;
    @RequestMapping("/toList")
    public String  toList(){
        return "user/toList";
    }
    @RequestMapping("/list")
    @ResponseBody
    public TableData<UserRoleVO> list(String uname,String sex,String name,Integer page,Integer limit){
        TableData<UserRoleVO> tableData = new TableData<UserRoleVO>();
        List<UserRoleVO> users = userService.listAll(uname,sex,name,page,limit);
        Integer count = userService.numberOfEmployees();
        tableData.setCount(count);
        tableData.setData(users);
        return tableData;
    }

    @RequestMapping("/toAdd")
    public String toEdit(){

        return "user/add";
    }

    /**
     * 保存或者修改用户的信息
     * @return
     */
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public DefaultMsg saveOrUpdate(User user,Long rid){

        DefaultMsg defaultMsg = null;
        try {
            defaultMsg = userService.saveOrUpdate(user,rid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defaultMsg;
    }
    //查询所有职业
    @RequestMapping("/listAllRole")
    @ResponseBody
    public List<UserRoleVO> listAllRole(){
        List<UserRoleVO> roles = userService.listAllRole();
        return roles;
    }
    @RequestMapping("/upload")
    @ResponseBody
    public DefaultMsgP upload(MultipartFile file){
        String saveName = UUID.randomUUID().toString();
        File saveFile = new File("E:/images/"+saveName);
        DefaultMsgP defaultMsgp = new DefaultMsgP();
        try {
            file.transferTo(saveFile);
            defaultMsgp.setTarget(saveName);
        } catch (IOException e) {
            defaultMsgp.setError("上传文件失败");
            e.printStackTrace();
        }
        return  defaultMsgp;
    }

    /**
     * 列出所有的角色
     * @return
     */
    @RequestMapping("/listAllRoles")
    @ResponseBody
    public List<Role> listAllRoles(){
        return roleService.listAll();
    }

    /**
     * 列出当前用户具有的角色
     * @return
     */
    @RequestMapping("/listUserRoles")
    @ResponseBody
    public List<Role> listUserRoles(Long id){
        return roleService.listUserRoles(id);
    }

    //修改个人信息
    @RequestMapping("/update")
    @ResponseBody
    public DefaultMsg update(User user){
        DefaultMsg update = userService.update(user);
        return update;
    }
}
