package com.aaa.controller;

import com.aaa.entity.Comments;
import com.aaa.service.CommentsService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/comments")
public class CommentsController {
    @Autowired
    private CommentsService commentsService;

    @RequestMapping("/toList")
    private String toList(){
        return "comments/list";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public DefaultMsg delete(Integer id){
        DefaultMsg defaultMsg=commentsService.delete(id);
        return defaultMsg;
    }


    @RequestMapping("/listData")
    @ResponseBody
    public TableData<Comments> listData(@RequestParam(defaultValue = "")String keyword, Integer page, Integer limit){
        TableData<Comments> tableData = commentsService.page(keyword, page, limit);
        return tableData;
    }
}
