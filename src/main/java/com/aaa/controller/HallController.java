package com.aaa.controller;

import com.aaa.entity.Hall;
import com.aaa.service.HallService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/hall")
public class HallController {
    @Autowired
    private HallService hallService;
    @RequestMapping("/toList")
    public String toList(){
        return "hall/list";
    }
    @RequestMapping("/toAdd")
    public String toAdd(){
        return "hall/add";
    }
    /**
     * 获取所有的商品数据
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public TableData<Hall> listData(@RequestParam(defaultValue = "") String keyword, Integer page, Integer limit){
        TableData<Hall> tableData = hallService.page(keyword,page, limit);
        return  tableData;
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public DefaultMsg saveOrUpdate(Hall hall){
        DefaultMsg defaultMsg = hallService.saveOrUpdate(hall);
        return defaultMsg;
    }


    @RequestMapping("/checkName")
    @ResponseBody
    public DefaultMsg checkName(String hname,Integer hid){
        DefaultMsg defaultMsg = hallService.checkName(hname,hid);
        return defaultMsg;
    }

    @RequestMapping("/delete")
    @ResponseBody
    public DefaultMsg delete(Long hid){
        DefaultMsg delete = hallService.delete(hid);
        return delete;
    }
}
