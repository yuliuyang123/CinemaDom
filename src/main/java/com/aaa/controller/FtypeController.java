package com.aaa.controller;

import com.aaa.entity.Ftype;
import com.aaa.service.FtypeService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ftype")
public class FtypeController {
    @Autowired
    private FtypeService ftypeService;


    @RequestMapping("/toList")
    public String toList(){
        return "ftype/list";
    }


    @RequestMapping("/listData")
    @ResponseBody
    public TableData<Ftype> listData(@RequestParam(defaultValue = "") String keyword, Integer page, Integer limit){
        TableData<Ftype> tableData=ftypeService.page(keyword,page,limit);
        return tableData;
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public DefaultMsg saveOrUpdate(Ftype ftype){
        DefaultMsg defaultMsg=ftypeService.saveOrUpdate(ftype);
        return defaultMsg;
    }



    @RequestMapping("/toEdit")
    public String toEdit(){
        return "ftype/edit";
    }



    @RequestMapping("/checkName")
    @ResponseBody
    public DefaultMsg checkName(String name,Integer tid){
        DefaultMsg defaultMsg = ftypeService.checkName(name,tid);
        return defaultMsg;
    }
}
