package com.aaa.controller;

import com.aaa.entity.Ptype;
import com.aaa.service.PtypeService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ptype")
public class PtypeController {
    @Autowired
    private PtypeService ptypeService;
    //跳转到 商品类别  jsp
    @RequestMapping("/toList")
    public String ptyprlist(){
        return "ptype/list";
    }

    /**
     *    查询
     * @param keyword
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/pListAll")
    @ResponseBody
    public TableData<Ptype> pListAll(@RequestParam(defaultValue = "")String keyword, Integer page, Integer limit){
        TableData<Ptype> ptypeTableData = ptypeService.ptypeList(keyword, page, limit);
        return ptypeTableData;
    }

    /**
     * 商品   添加和修改
     */
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public DefaultMsg saveOrUpdate(Ptype ptype){
        DefaultMsg defaultMsg = ptypeService.saveOrupdate(ptype);
        return defaultMsg;
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @ResponseBody
    public DefaultMsg delete(Integer tid){
        DefaultMsg delete = ptypeService.delete(tid);
        return delete;
    }

    @RequestMapping("/toEdit")
    public String toEdit(){
        return "ptype/edit";
    }
}
