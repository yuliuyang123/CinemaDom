package com.aaa.controller;


import com.aaa.entity.Order;
import com.aaa.entity.OrderDetail;
import com.aaa.entity.User;
import com.aaa.service.OrderService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.ProductUtil;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
@RequestMapping("/orderdetil")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping("/oList")
    public String oList(){
        return "orderdetil/list";
    }

/*    @RequestMapping("/toDetail")
    public String toDetail(){
        return "order/detail";
    }*/

    @RequestMapping("/dList")
    public String dList(Long oid, Model model){
        model.addAttribute("oid",oid);
        return "orderdetil/detail";
    }



    @RequestMapping("/listOrder")
    @ResponseBody
    public TableData<Order> listOrder(@RequestParam(defaultValue = "") String keyword, Integer page, Integer limit, HttpSession httpSession){
        User user = (User)httpSession.getAttribute("USER");
        TableData<Order> tableData = orderService.page(keyword,page, limit);
        return tableData;
    }

    @RequestMapping("/listDetail")
    @ResponseBody
    public TableData<OrderDetail> listDetail(@RequestParam(defaultValue = "") String keyword, Integer page, Integer limit ,HttpSession httpSession,Long oid){
        TableData<OrderDetail> tableData = orderService.pageDetail(keyword, page, limit,oid);
        return tableData;
    }

    @RequestMapping("/save")
    @ResponseBody
    public DefaultMsg save(String str, HttpSession httpSession){
        User user = (User)httpSession.getAttribute("USER");
        ArrayList<ProductUtil> productUtils = new ArrayList<ProductUtil>();
        String[] split = str.split(",");
        for (int i = 0; i < split.length; i++){
            System.out.println(split[i]);
            String[] strs = split[i].split(":");
            ProductUtil productUtil = new ProductUtil();
            productUtil.setPid(Long.valueOf(strs[0]));
            productUtil.setUnit_price(Double.valueOf(strs[1]));
            productUtil.setStock(Integer.valueOf(strs[2]));
            productUtils.add(productUtil);
        }
        DefaultMsg defaultMsg = orderService.save(productUtils);
        return defaultMsg;
    }
}
