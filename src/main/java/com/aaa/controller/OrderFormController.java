package com.aaa.controller;

import com.aaa.entity.*;
import com.aaa.service.OrderFormService;
import com.aaa.service.StatisticsService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderFormController {

    @Autowired
    private OrderFormService orderFormService;
    @Autowired
    private StatisticsService statisticsService;

    @RequestMapping("/toList")
    public String toList(){
        return "order/list";
    }
    @RequestMapping("/listAll")
    @ResponseBody
    public TableData<OrderForm> listAll(@RequestParam(defaultValue = "") String keyword, Integer page, Integer limit){
        TableData<OrderForm> orderFormTableData = orderFormService.listAll(keyword, page, limit);
        return orderFormTableData;
    }

    @RequestMapping("/save")
    @ResponseBody
    public DefaultMsg save(String buy_seats,Long tid,String price,String luid,Long numseat,Long fid){
        OrderForm orderForm = new OrderForm();
        orderForm.setBuy_seats(buy_seats);
        TicketSales ticketSales = new TicketSales();
        ticketSales.setTid(tid);
        orderForm.setTicketSales(ticketSales);
        orderForm.setPrice(Double.valueOf(price));
        User user = new User();
        user.setUid(Long.valueOf(luid));
        orderForm.setLuid(user);
        DefaultMsg defaultMsg = orderFormService.save(orderForm);
        orderFormService.update(numseat,tid);
        Statistics statistics = new Statistics();
        Film film = new Film();
        film.setId(fid);
        statistics.setFilm(film);
        statistics.setZprice(Double.valueOf(price));
        statistics.setZquantity(numseat);
        statisticsService.saveOrUpdate(statistics);
        return defaultMsg;
    }

}
