package com.aaa.controller;

import com.aaa.dao.HallDao;
import com.aaa.dao.OrderFormDao;
import com.aaa.dao.TicketSalesDao;
import com.aaa.entity.Film;
import com.aaa.entity.Hall;
import com.aaa.entity.OrderForm;
import com.aaa.entity.TicketSales;
import com.aaa.service.FilmService;
import com.aaa.service.TicketSalesService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
//拍片控制器
@Controller
@RequestMapping("/ticketSales")
public class TicketSalesController {
    @Autowired
    private TicketSalesService ticketSalesService;
    @Autowired
    private TicketSalesDao ticketSalesDao;

    @Autowired
    private FilmService filmService;

    @RequestMapping("/toList")
    public String toList(){
        return "ticketsales/list";
    }
    @RequestMapping("/toEdit")
    public String toAdd(){
        return "ticketsales/edit";
    }
    /**
     * 获取所有的场次数据
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public TableData<TicketSales> listData(@RequestParam(defaultValue = "")String keyword,  Integer page, Integer limit){
        TableData<TicketSales> tableData = ticketSalesService.page(keyword,page, limit);
        return  tableData;
    }
    /**
     * 获取所有的影厅数据
     * @return
     */
    @RequestMapping("/listHall")
    @ResponseBody
    public List<Hall> listHall(){
        List<Hall> hall = ticketSalesDao.listHall();
        return hall;
    }
    //listFilm查询所有影厅数据
    @RequestMapping("/listFilm")
    @ResponseBody
    public List<Film> listFilm(){
        List<Film> film = ticketSalesDao.listFilm();
        return film;
    }
    //根据选中的电影id查询出电影时长
    @RequestMapping("/listFilmByid")
    @ResponseBody
    public Film listFilmByid(Long id){
        Film film = ticketSalesDao.listFilmByid(id);
        return film;
    }
    @RequestMapping("/listHallByHid")
    @ResponseBody
    public Hall listHallByHid(Long hid){
        Hall hall = ticketSalesDao.listHallByHid(hid);
        return hall;
    }
    @RequestMapping("/save")
    @ResponseBody
    public DefaultMsg save(TicketSales ticketSales,String remaining_votes){
        ticketSales.setRemaining_votes(remaining_votes);
        DefaultMsg defaultMsg=ticketSalesService.save(ticketSales);
        return defaultMsg;
    }
    /**
     * 校验场次时间是否重复
     * @return
     */
    @RequestMapping("/checkSales")
    @ResponseBody
    public DefaultMsg  checkSales(String opening_time,String break_time,Long hid){
        DefaultMsg defaultMsg = ticketSalesService.checkSales(opening_time,break_time,hid);
        return defaultMsg;
    }


    /**
     * 查询所有的电影信息
     * @return
     */
    @RequestMapping("/listAllFilm")
    @ResponseBody
    public List<Film> listAllFilm(){
        List<Film> films = filmService.listAll();
        return films;
    }




    /**
     *  根据商品编号删除商品信息
     * @param tid
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public DefaultMsg delete(Long tid){
        DefaultMsg defaultMsg = ticketSalesService.delete(tid);
        return  defaultMsg;
    }

}
