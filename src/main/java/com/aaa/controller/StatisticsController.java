package com.aaa.controller;

import com.aaa.entity.Statistics;
import com.aaa.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/statistics")
public class StatisticsController {
    @Autowired
    private StatisticsService statisticsService;
    @RequestMapping("/toList")
    public String toList(){
        return "statistics/list";
    }

    @RequestMapping("/listTop10")
    @ResponseBody
    public List<Statistics> listTop10(){
        List<Statistics> statistics = statisticsService.listTop10();
        return statistics;
    }
}
