package com.aaa.controller;

import com.aaa.entity.Identity;
import com.aaa.entity.LeadingUser;
import com.aaa.service.IdentityService;
import com.aaa.service.LeadingUserService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.DefaultMsgP;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/leadingUser")
public class LeadingUserController {
    @Autowired
    private LeadingUserService leadingUserService;


    @Autowired
    private IdentityService identityService;



    @RequestMapping("/toList")
    private String toList(){
        return "leadingUser/list";
    }
    @RequestMapping("/listData")
    @ResponseBody
    public TableData<LeadingUser>listData(@RequestParam(defaultValue = "")String keyword,Integer page,Integer limit){
        TableData<LeadingUser> tableData = leadingUserService.page(keyword, page, limit);
        return tableData;
    }

    @RequestMapping("/identity")
    @ResponseBody
    public DefaultMsg identity(LeadingUser leadingUser,Long id){
        DefaultMsg identity = leadingUserService.identity(leadingUser, id);
        return identity;
    }

    @RequestMapping("/upload")
    @ResponseBody
    public DefaultMsgP upload(MultipartFile file){
        String saveName= UUID.randomUUID().toString();
        File saveFile = new File("D:/upload/" + saveName);
        DefaultMsgP defaultMsg=new DefaultMsgP();

        try {
            file.transferTo(saveFile);
            defaultMsg.setTarget(saveName);
        } catch (IOException e) {
            defaultMsg.setError("上传文件失败");
            e.printStackTrace();
        }
        return defaultMsg;
    }



    @RequestMapping("/identitylistAll")
    @ResponseBody
    private List<Identity> test(){
        List<Identity> identities = identityService.listAll();
        return identities;
    }
}
