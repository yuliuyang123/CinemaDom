package com.aaa.controller;

import com.aaa.entity.Product;
import com.aaa.service.ProductService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/cell")
public class CellController {
    @Autowired
    private ProductService productService;

    @RequestMapping("/toList")
    public String toList(){
        return "product/sell";
    }

    @RequestMapping("/listData")
    @ResponseBody
    public TableData<Product> listData(@RequestParam(defaultValue = "") String keyword, Integer page, Integer limit){
        TableData<Product> tableData = productService.page(keyword, page, limit);
        return tableData;
    }
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public DefaultMsg saveOrUpdate(Product product){
        DefaultMsg defaultMsg = productService.saveOrUpdate(product);
        return defaultMsg;

    }


    /**
     *  根据商品编号删除商品信息
     * @param pid
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public DefaultMsg delete(Long pid){
        DefaultMsg defaultMsg = productService.delete(pid);
        return  defaultMsg;
    }



}
