package com.aaa.dao;

import com.aaa.entity.Identity;
import com.aaa.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class IdentityDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Identity> rowMapper=new BeanPropertyRowMapper<Identity>(Identity.class);


    public List<Identity> listAll(){
        String  sql="select * from identity";
        List<Identity> query = jdbcTemplate.query(sql, rowMapper);
        return query;
    }
}
