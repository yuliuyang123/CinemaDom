package com.aaa.dao;


import com.aaa.entity.Order;
import com.aaa.entity.OrderDetail;
import com.aaa.entity.Product;
import com.aaa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class OrderDetailDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<OrderDetail> rowMapper = new RowMapper<OrderDetail>() {
        public OrderDetail mapRow(ResultSet resultSet, int i) throws SQLException {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setId(resultSet.getLong("id"));
            orderDetail.setStock(resultSet.getInt("stock"));
            orderDetail.setPrice(resultSet.getDouble("price"));
            orderDetail.setSubtotal(resultSet.getDouble("subtotal"));
            Product product = new Product();
            product.setPid(resultSet.getLong("pid"));
            product.setPname(resultSet.getString("pname"));
            product.setImg_product(resultSet.getString("img_product"));
            Order order = new Order();
            order.setId(resultSet.getLong("id"));
            order.setOrder_number(resultSet.getString("order_number"));
            orderDetail.setProduct(product);
            orderDetail.setOrder(order);
            return orderDetail;
        }
    };

/*    public List<OrderDetail> listPage(String keyword, Integer offset, Integer limit) {
        String sql = "select d.id,o.id as uid,o.order_number,p.pname,d.stock,d.price,d.subtotal,d.pid,p.img_product from product p " +
                "inner join orderdetail d on p.pid = d.pid " +
                "inner join orders o on o.order_number = d.oid " +
                "where o.order_number like ? limit ?,?";
        List<OrderDetail> result = this.jdbcTemplate.query(sql, rowMapper,"%" + keyword + "%", offset, limit);
        return result;
    }*/

/*    public List<OrderDetail> listPage(Integer offset, Integer limit,User user) {
        String sql = "select d.id,o.id as uid,o.order_number,p.pname,d.stock,d.price,d.subtotal,d.pid,p.img_product from product p " +
                "inner join orderdetail d on p.pid = d.pid " +
                "inner join orders o on o.order_number = d.oid " +
                "where o.uid = ? order by d.id desc limit ?,?";
        List<OrderDetail> result = this.jdbcTemplate.query(sql, rowMapper,user.getId(), offset, limit);
        return result;
    }*/

    public List<OrderDetail> listPage(Integer offset, Integer limit,Long oid) {
        String sql = "select d.id,o.id as uid,o.order_number,p.pname,d.stock,d.price,d.subtotal,d.pid,p.img_product from product p " +
                "inner join orderdetail d on p.pid = d.pid " +
                "inner join orders o on o.order_number = d.oid " +
                "where d.oid = ? order by d.id desc limit ?,?";
        List<OrderDetail> result = this.jdbcTemplate.query(sql, rowMapper,oid, offset, limit);
        return result;
    }

/*    public Integer count(User user) {
        String sql = "select count(id) as c from orderdetail where oid = ?";
        Integer result = this.jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("c");
            }
        }, user.getId());
        return result;
    }*/

    public Integer count(String keyword) {
        String sql = "select count(id) as c from orderdetail where oid like ?";
        Integer result = this.jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("c");
            }
        }, "%" + keyword + "%");
        return result;
    }

    public Integer save(OrderDetail orderDetail) {
        String sql = "insert into orderdetail(oid,pid,stock,price,subtotal) values(?,?,?,?,?)";
        int count = this.jdbcTemplate.update(sql, orderDetail.getOrder().getOrder_number(),orderDetail.getProduct().getPid(),orderDetail.getStock(), orderDetail.getPrice(), orderDetail.getSubtotal());
        return count;
    }
}
