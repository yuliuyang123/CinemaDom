package com.aaa.dao;

import com.aaa.entity.Identity;
import com.aaa.entity.LeadingUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class LeadingUserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private RowMapper<LeadingUser> rowMapper=new RowMapper<LeadingUser>() {
        public LeadingUser mapRow(ResultSet resultSet, int i) throws SQLException {
            LeadingUser leadingUser=new LeadingUser();
            leadingUser.setLid(resultSet.getLong("lid"));
            leadingUser.setLuname(resultSet.getString("luname"));
            leadingUser.setPassword(resultSet.getString("password"));
            leadingUser.setPhone(resultSet.getString("phone"));
            leadingUser.setPicture(resultSet.getString("picture"));
            leadingUser.setCreate_time(resultSet.getString("create_time"));

            Identity identity=new Identity();
            identity.setId(resultSet.getLong("id"));
            identity.setIname(resultSet.getString("iname"));
            leadingUser.setIdentity(identity);
            return leadingUser;
        }
    };


    public List<LeadingUser> listPage(String keyword,Integer offset,Integer limit){
        String sql="select l.lid,l.luname,l.password,l.phone,l.picture,l.create_time,i.id,i.iname from leading_user l inner join identity i on l.id=i.id where l.luname like ? order by create_time desc limit ?,?";
        List<LeadingUser> result = jdbcTemplate.query(sql, rowMapper, "%" + keyword + "%", offset, limit);
        return result;
    }


    public Integer count(String keyword){
        String sql="select count(lid) as c from leading_user where luname like ?";
        final Integer result=jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("c");
            }
        },"%"+keyword+"%");
        return result;
    }


    public Integer integer(LeadingUser leadingUser,Long lid){
        String sql="update leading_user set id=? where lid=?";
        int count = jdbcTemplate.update(sql, lid, leadingUser.getLid());
        return count;
    }
}

