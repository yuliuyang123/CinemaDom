package com.aaa.dao;

import com.aaa.entity.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 菜单管理dao实现类
 */
@Repository
public class MenuDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Menu> rowMapper = new RowMapper<Menu>() {
        public Menu mapRow(ResultSet resultSet, int i) throws SQLException {
            Menu menu = new Menu();
            menu.setId(resultSet.getLong("id"));
            menu.setName(resultSet.getString("name"));
            menu.setPath(resultSet.getString("path"));
            menu.setLev(resultSet.getInt("lev"));
            menu.setNamespace(resultSet.getString("namespace"));
            return menu;
        }
    };
    /**
     * 根据用户id查询用户能操作的菜单
     * @param uid
     * @return
     */
    public List<Menu> listMenusByUid(Long uid){
        String sql = "select distinct m.* from user u inner join  user_role ur on u.uid=ur.u_id\n" +
                "                       inner join  role r on r.id=ur.r_id\n" +
                "                       inner join  role_menu rm on r.id=rm.r_id\n" +
                "                       inner join  menu m on m.id=rm.m_id\n" +
                "                       where u.uid=?";
        List<Menu> result= this.jdbcTemplate.query(sql, rowMapper, uid);
        return result;

    }

    /**
     * 查询用户能操作的一级菜单
     * @return
     */
    public List<Menu> listOneMenus(Long uid){
        String sql = "select distinct m.* from user u inner join  user_role ur on u.uid=ur.u_id\n" +
                "                       inner join  role r on r.id=ur.r_id\n" +
                "                       inner join  role_menu rm on r.id=rm.r_id\n" +
                "                       inner join  menu m on m.id=rm.m_id\n" +
                "                       where u.uid=? and m.lev=1 order by m.id";
        List<Menu> result= this.jdbcTemplate.query(sql, rowMapper, uid);
        return result;


    }

    /**
     * 查询用户能操作的某个一级菜单下的二级菜单
     * @return
     */
    public List<Menu> listTwoMenus(Long uid,Long pid){
        String sql = "select distinct m.* from user u inner join  user_role ur on u.uid=ur.u_id\n" +
                "                       inner join  role r on r.id=ur.r_id\n" +
                "                       inner join  role_menu rm on r.id=rm.r_id\n" +
                "                       inner join  menu m on m.id=rm.m_id\n" +
                "                       where u.uid=? and m.lev=2 and m.pid=? order by m.id";
        List<Menu> result= this.jdbcTemplate.query(sql, rowMapper, uid,pid);
        return result;


    }
    /**
     * 查询所有的一级菜单
     * @return
     */
    public List<Menu> listOneMenus(){
        String sql = "select distinct m.* from user u inner join  user_role ur on u.uid=ur.u_id\n" +
                "                       inner join  role r on r.id=ur.r_id\n" +
                "                       inner join  role_menu rm on r.id=rm.r_id\n" +
                "                       inner join  menu m on m.id=rm.m_id\n" +
                "                       where  m.lev=1 order by m.id ";
        List<Menu> result= this.jdbcTemplate.query(sql, rowMapper);
        return result;


    }
    /**
     * 查询某个一级菜单下的二级菜单
     * @return
     */
    public List<Menu> listTwoMenus(Long pid){
        String sql = "select distinct m.* from user u inner join  user_role ur on u.uid=ur.u_id\n" +
                "                       inner join  role r on r.id=ur.r_id\n" +
                "                       inner join  role_menu rm on r.id=rm.r_id\n" +
                "                       inner join  menu m on m.id=rm.m_id\n" +
                "                       where m.lev=2 and m.pid=?";
        List<Menu> result= this.jdbcTemplate.query(sql, rowMapper,pid);
        return result;


    }

    /**
     * 查询角色能操作的所有菜单
     * @param rid
     * @return
     */
    public List<Menu> listRoleMenus(Long rid) {
        String sql = "select m.* from role_menu rm inner join menu m on rm.m_id=m.id where rm.r_id=?";
        List<Menu> result= this.jdbcTemplate.query(sql, rowMapper,rid);
        return result;
    }
}
