package com.aaa.dao;

import com.aaa.entity.Film;
import com.aaa.entity.Hall;
import com.aaa.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 影厅管理dao实现类
 */
@Repository
public class HallDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    public Hall checkName(Hall hall){
        String sql="select * from hall where hname=? ";
        List<Hall> result = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Hall>(Hall.class),hall.getHname());
        if (result.size()>0){
            return result.get(0);
        }else {
            return null;
        }

    }

    //查询所有影厅信息
    public List<Hall> listPage(String keyword, Integer offset, Integer limit){
        String sql = "select * from hall where hname like ? order by create_date  desc limit ?,? ";
        List<Hall> result = this.jdbcTemplate.query(sql,new BeanPropertyRowMapper<Hall>(Hall.class),"%"+keyword+"%", offset, limit);
        return  result;
    }
    /**
     * 查询总条数
     * @return
     */
    public Integer count(String keyword){
        String sql = "select count(hid) from hall where hname like ? ";
        Integer integer = this.jdbcTemplate.queryForObject(sql, Integer.class, "%" + keyword + "%");
        return  integer;
    }
    /**
     * 保存影厅信息
     * @param hall
     * @return
     */
    public Integer save(Hall hall){
        String sql = "insert into hall (hname,hang,lie,seat,create_date,detailed) values(?,?,?,?,?,?)";
        int count = this.jdbcTemplate.update(sql,hall.getHname(),hall.getHang(),hall.getLie(),hall.getSeat(), hall.getCreate_date(),hall.getDetailed());
        return  count;
    }

    /**
     * 修改影厅信息
     * @param hall
     * @return
     */
    public Integer update(Hall hall){
        String sql = "update hall set hname=?,hang=?,lie=?,seat,detailed=? where hid=?";
        int count = this.jdbcTemplate.update(sql,hall.getHname(),hall.getHang(),hall.getLie(),hall.getSeat(),hall.getHid(),hall.getDetailed());
        return count;
    }

    /**
     * 删除影厅信息
     * @param hid
     * @return
     */
    public Integer delete(Long hid){
        String sql = "delete from hall where hid =?";
        int count = this.jdbcTemplate.update(sql,hid);
        return count;
    }



    public Integer countByName(String hname){
        String sql = "select count(hid) as c from hall where hname = ? ";
        Integer result = this.jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("c");
            }
        },hname);
        return  result;
    }

    public Integer countByName(String hname,Integer hid){
        String sql = "select count(hid) as c from hall where hname = ? and id != ? ";
        Integer result = this.jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("c");
            }
        },hname,hid);
        return  result;
    }
}
