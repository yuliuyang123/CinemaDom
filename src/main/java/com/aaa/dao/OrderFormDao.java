package com.aaa.dao;

import com.aaa.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class OrderFormDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private RowMapper<OrderForm> rowMapper = new RowMapper<OrderForm>() {

        public OrderForm mapRow(ResultSet resultSet, int i) throws SQLException {
            OrderForm orderForm = new OrderForm();
            orderForm.setOid(resultSet.getLong("oid"));
            orderForm.setOrdeId(resultSet.getLong("ordeId"));
            orderForm.setTicket_code(resultSet.getLong("ticket_code"));
            orderForm.setBooking_time(resultSet.getString("booking_time"));
            orderForm.setBuy_seats(resultSet.getString("buy_seats"));
            orderForm.setPrice(resultSet.getDouble("price"));
            TicketSales ticketSales = new TicketSales();
            ticketSales.setTid(resultSet.getLong("tid"));
            ticketSales.setOpening_time(resultSet.getString("opening_time"));
            ticketSales.setBreak_time(resultSet.getString("break_time"));
            Film film=new Film();
            film.setId(resultSet.getLong("fid"));
            film.setChinese_name(resultSet.getString("chinese_name"));
            film.setImgUrl(resultSet.getString("img_url"));
            film.setDuration(resultSet.getString("duration"));
            film.setCountry(resultSet.getString("country"));
//            Ftype ftype=new Ftype();
//            ftype.setTid(resultSet.getLong("tid"));
//            ftype.setName(resultSet.getString("name"));
//            film.setFtype(ftype);
            ticketSales.setFilm(film);
            Hall hall = new Hall();
            hall.setHid(resultSet.getLong("hid"));
            hall.setHname(resultSet.getString("hname"));
            ticketSales.setHall(hall);
            orderForm.setTicketSales(ticketSales);
            Identity identity = new Identity();
            identity.setId(resultSet.getLong("id"));
            identity.setIname(resultSet.getString("iname"));
            orderForm.setState(identity);
            User user = new User();
            user.setUid(resultSet.getLong("uid"));
            user.setUname(resultSet.getString("uname"));
            orderForm.setLuid(user);
            return orderForm;
        }
    };
    //查询当前场次的所有订单
    public List<OrderForm> listByTid(Long tid){
        String sql = "select * from order_form o join ticket_sales t on o.tid =t.tid  join film f on t.fid = f.id join hall h on h.hid=t.hid join user u on o.luid = u.uid join identity i on o.state=i.id where o.tid = ?";
        List<OrderForm> query = this.jdbcTemplate.query(sql, rowMapper, tid);
        return query;
    }
    //查询所有订单
    public List<OrderForm> listAll(String keyword,Integer offset, Integer limit){
        String sql = "select * from order_form o join ticket_sales t on o.tid =t.tid  join film f on t.fid = f.id join hall h on h.hid=t.hid join user u on o.luid = u.uid join identity i on o.state=i.id where booking_time like ? ORDER BY booking_time desc limit ?,?";
        List<OrderForm> query = this.jdbcTemplate.query(sql, rowMapper,"%"+keyword+"%" ,offset, limit);
        return query;
    }
    /**
     * 查询总条数
     * @param keyword
     * @return
     */
    public Integer count(String keyword){
        String sql="select count(oid) as c from order_form where booking_time like ?";
        final Integer result=jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("c");
            }
        },"%"+keyword+"%");
        return result;
    }
    //添加订单
    public Integer save(OrderForm orderForm){
        String sql="insert into order_form(ordeId,ticket_code,buy_seats,booking_time,state,price,tid,luid) values(?,?,?,?,?,?,?,?)";
        int count = jdbcTemplate.update(sql,orderForm.getOrdeId(),orderForm.getTicket_code(),orderForm.getBuy_seats(),orderForm.getBooking_time(),orderForm.getState().getId(),orderForm.getPrice(),orderForm.getTicketSales().getTid(),orderForm.getLuid().getUid());
        return count;
    }
    //修改剩余票数
    public Integer update(Long numseat,Long tid){
        String sql = "update ticket_sales set  remaining_votes=(remaining_votes-?) where tid = ?";
        int update = this.jdbcTemplate.update(sql, numseat, tid);
        return update;
    }
}
