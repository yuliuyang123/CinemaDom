package com.aaa.dao;


import com.aaa.entity.Order;
import com.aaa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class OrderDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Order> rowMapper = new RowMapper<Order>() {
        public Order mapRow(ResultSet resultSet, int i) throws SQLException {
            Order order = new Order();
            order.setId(resultSet.getLong("id"));
            order.setOrder_number(resultSet.getString("order_number"));
            order.setCreate_date(resultSet.getString("create_date"));
            order.setTotal(resultSet.getDouble("total"));
            return order;
        }
    };


    public List<Order> listPage(String keyword,Integer offset, Integer limit) {
        String sql = "select o.id,o.order_number,o.create_date,o.total from orders o  where o.order_number like ?  order by o.id desc limit ?,?";
        List<Order> result = this.jdbcTemplate.query(sql, rowMapper,"%" + keyword + "%",offset, limit);
        return result;
    }


    public Integer count() {
        String sql = "select count(id) as c from orders";
        Integer result = this.jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("c");
            }
        });
        return result;
    }

    public Integer save(Order order) {
        String sql = "insert into orders(order_number,create_date,total) values(?,?,?)";
        int count = this.jdbcTemplate.update(sql,order.getOrder_number(),order.getCreate_date(),order.getTotal());
        return count;
    }
}
