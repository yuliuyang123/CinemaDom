package com.aaa.dao;

import com.aaa.entity.Film;
import com.aaa.entity.Ftype;
import com.aaa.entity.Hall;
import com.aaa.entity.TicketSales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class TicketSalesDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<TicketSales> rowMapper = new RowMapper<TicketSales>() {
        public TicketSales mapRow(ResultSet resultSet, int i) throws SQLException {
            TicketSales ticketSales = new TicketSales();
            ticketSales.setTid(resultSet.getLong("tid"));
            ticketSales.setOpening_time(resultSet.getString("opening_time"));
            ticketSales.setBreak_time(resultSet.getString("break_time"));
            ticketSales.setCreation_time(resultSet.getString("creation_time"));
            ticketSales.setRemaining_votes(resultSet.getString("remaining_votes"));
            ticketSales.setPrice(resultSet.getDouble("price"));
            Film film=new Film();
            film.setId(resultSet.getLong("fid"));
            film.setChinese_name(resultSet.getString("chinese_name"));
            film.setEnglish_name(resultSet.getString("english_name"));
            film.setImgUrl(resultSet.getString("img_url"));
            film.setDuration(resultSet.getString("duration"));
            film.setShowtimes(resultSet.getString("showtimes"));
            film.setIntroduction(resultSet.getString("introduction"));
            film.setCountry(resultSet.getString("country"));
            film.setDirector(resultSet.getString("director"));
            film.setActors(resultSet.getString("actors"));
            Ftype ftype=new Ftype();
            ftype.setTid(resultSet.getLong("tid"));
            ftype.setName(resultSet.getString("name"));
            film.setFtype(ftype);
            ticketSales.setFilm(film);
            Hall hall = new Hall();
            hall.setHid(resultSet.getLong("hid"));
            hall.setHname(resultSet.getString("hname"));
            hall.setHang(resultSet.getString("hang"));
            hall.setSeat(resultSet.getString("seat"));
            hall.setLie(resultSet.getString("lie"));
            hall.setCancel(resultSet.getString("cancel"));
            hall.setDetailed(resultSet.getString("detailed"));
            ticketSales.setHall(hall);
            return ticketSales;
        }
    };

    public List<TicketSales> listPage(String keyword, Integer offset, Integer limit){
        String sql = "select * from ticket_sales t join film f on t.fid =f.id join ftype ft on f.tid = ft.tid join hall h on h.hid = t.hid\n" +
                     "where f.chinese_name like ? order by creation_time  desc limit ?,? ";
        List<TicketSales> result = this.jdbcTemplate.query(sql, rowMapper,"%"+keyword+"%", offset, limit);
        return  result;
    }
    /**
     * 查询总条数
     * @return
     */
    public Integer count(){
        String sql = "select count(tid) from ticket_sales  ";
        Integer integer = this.jdbcTemplate.queryForObject(sql, Integer.class);
        return  integer;
    }
    //查询所有影厅信息
    public List<Hall> listHall(){
        String sql = "select * from hall ";
        List<Hall> result = this.jdbcTemplate.query(sql,new BeanPropertyRowMapper<Hall>(Hall.class));
        return  result;
    }
    //查询所有电影的信息

    public List<Film> listFilm(){
        String sql="select * from film ";
        List<Film> query=jdbcTemplate.query(sql,new BeanPropertyRowMapper<Film>(Film.class));
        return query;
    }

    //根据id查询电影的时长

    public Film listFilmByid(Long id){
        String sql="select * from film where id = ? ";
        Film film = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<Film>(Film.class), id);
        return film;
    }
    //根据hid查询影厅的座位数
    public Hall listHallByHid(Long hid) {
        String sql="select * from hall where hid = ? ";
        Hall hall = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<Hall>(Hall.class), hid);
        return hall;
    }
    /**
     * 保存拍片信息
     * @param ticketSales
     * @return
     */
    public Integer save(TicketSales ticketSales){
        String sql="insert into ticket_sales(opening_time,break_time,remaining_votes,price,fid,hid,creation_time,sid) values(?,?,?,?,?,?,?,?)";
        int count = jdbcTemplate.update(sql,ticketSales.getOpening_time(),ticketSales.getBreak_time(),ticketSales.getRemaining_votes(),ticketSales.getPrice(),ticketSales.getFilm().getId(),ticketSales.getHall().getHid(),ticketSales.getCreation_time(),ticketSales.getSid());
        return count;
    }

    //查询所有拍片是否重复

    public Integer checkSales(String opening_time,String break_time,Long hid){
        String sql="select count(tid) as c from ticket_sales " +
                "where  hid = ? and DATE_FORMAT(opening_time, '%Y-%m-%d %H:%i') " +
                "<= DATE_FORMAT(?, '%Y-%m-%d %H:%i') and " +
                "DATE_FORMAT(break_time, '%Y-%m-%d %H:%i') >=DATE_FORMAT(?, '%Y-%m-%d %H:%i') ";
        System.out.println(sql);
        Integer result = this.jdbcTemplate.queryForObject(sql, Integer.class,hid,break_time,opening_time);

        return  result;
    }

    //删除
    public Integer delete(Long tid){
        String sql="delete from ticket_sales where tid=?";
        int count = this.jdbcTemplate.update(sql, tid);
        return count;
    }

}

