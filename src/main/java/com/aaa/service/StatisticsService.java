package com.aaa.service;

import com.aaa.entity.Statistics;
import com.aaa.util.DefaultMsg;

import java.util.List;

//报表
public interface StatisticsService {
    Integer saveOrUpdate(Statistics statistics);
    List<Statistics> listTop10();
}
