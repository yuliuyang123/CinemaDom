package com.aaa.service;

import com.aaa.entity.Ptype;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

import java.util.List;

/**
 * 商品类别管理service接口
 */
public interface PtypeService {

    /**
     * 查询所有的商品类别
     * @return
     */
//商品类别表的查询
    public TableData<Ptype> ptypeList(String keyword, Integer page, Integer limit);

    public DefaultMsg saveOrupdate(Ptype ptype);
    public DefaultMsg delete(Integer tid);}
