package com.aaa.service;

import com.aaa.entity.LeadingUser;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

public interface LeadingUserService {
    public TableData<LeadingUser> page(String keyword,Integer page,Integer limit);


    public DefaultMsg identity(LeadingUser leadingUser,Long lid);
}
