package com.aaa.service;

import com.aaa.entity.TicketSales;
import com.aaa.util.TableData;

public interface SellTicketService {
     TableData<TicketSales> page(Long fid,Integer page, Integer limit);

}
