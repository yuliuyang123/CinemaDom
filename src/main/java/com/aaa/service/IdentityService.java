package com.aaa.service;

import com.aaa.entity.Identity;

import java.util.List;

public interface IdentityService {

    public List<Identity> listAll();
}
