package com.aaa.service;

import com.aaa.entity.Order;
import com.aaa.entity.OrderDetail;
import com.aaa.entity.User;
import com.aaa.util.ProductUtil;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

import java.util.List;

public interface OrderService {

    public TableData<Order> page(String keyword,Integer page,Integer limit);
    public TableData<OrderDetail> pageDetail(String keyword,Integer page, Integer limit,Long oid);
    public DefaultMsg save(List<ProductUtil> productUtils);
}
