package com.aaa.service;

import com.aaa.entity.Menu;

import java.util.List;
import java.util.Map;

public interface MenuService {
    /**
     * 根据用户id查询用户能操作的所有菜单
     * @param uid
     * @return
     */
    List<Menu> listMyMenus(Long uid);


    /**
     * 获取用户能操作的所有的二级菜单的路径
     * @param oneMenus
     * @return
     */
    List<String> convertMenus(List<Menu> oneMenus);

    /**
     * 根据用户id查询用户能操作的所有菜单
     * @param uid
     * @return
     */
    public Map<String,Object> listMiniUiMenus(Long uid);
    /**
     * 查询某个角色能操作的所有菜单
     * @return
     */
    public List<Menu> listRoleMenus(Long rid);
    /**
     * 按层级查出所有一级菜单和二级菜单
     * @return
     */
    public List<Menu> listAllByLevel();

}
