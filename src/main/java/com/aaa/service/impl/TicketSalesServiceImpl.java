package com.aaa.service.impl;

import com.aaa.dao.TicketSalesDao;
import com.aaa.entity.TicketSales;
import com.aaa.service.TicketSalesService;
import com.aaa.util.DateUtil;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketSalesServiceImpl implements TicketSalesService {
    @Autowired
    TicketSalesDao ticketSalesDao = new TicketSalesDao();
    public TableData<TicketSales> page(String keyword, Integer page, Integer limit) {
        TableData<TicketSales> tableData = new TableData<TicketSales>();
        //第一步，查询当前页的数据
        Integer offset = (page-1)*limit;
        List<TicketSales> data =ticketSalesDao.listPage(keyword,offset, limit);
        tableData.setData(data);
        //第二步，查询总条数
        Integer count = ticketSalesDao.count();
        tableData.setCount(count);
        return tableData;
    }

    public DefaultMsg save(TicketSales ticketSales) {
        DefaultMsg defaultMsg = new DefaultMsg();
        ticketSales.setCreation_time(DateUtil.getCurrentDate());
        ticketSales.setSid(Long.valueOf(1));
        Integer count = ticketSalesDao.save(ticketSales);
        if(count==0){
            defaultMsg.setSuccess(0);
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }

    public DefaultMsg checkSales(String opening_time,String break_time, Long hid) {
        //如果不重复 defaultMsg success=1 如果重复 success=0
        DefaultMsg defaultMsg = new DefaultMsg();

        Integer count = ticketSalesDao.checkSales(opening_time, break_time, hid);
        if(count>0){
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    public DefaultMsg delete(Long tid) {
        DefaultMsg defaultMsg=new DefaultMsg();

        Integer count = ticketSalesDao.delete(tid);
        if (count==0){
            defaultMsg.setError("删除失败");
        }
        return defaultMsg;
    }
}
