package com.aaa.service.impl;

import com.aaa.dao.PtypeDao;
import com.aaa.entity.Ptype;
import com.aaa.service.PtypeService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品类别管理service实现类
 */
@Service
public class PtypeServiceImpl implements PtypeService {

    @Autowired
    private PtypeDao ptypeDao;

    //  查询列表
    public TableData<Ptype> ptypeList(String keyword, Integer page, Integer limit) {
        TableData<Ptype> tableData=new TableData<Ptype>();
        //第一步，查询当前页的数据
        Integer offset=(page-1)*limit;
        List<Ptype> ptypes = ptypeDao.ptypeList(keyword, offset, limit);
        tableData.setData(ptypes);
        //第二步，查询总条数
        Integer count =ptypeDao.count(keyword);
        tableData.setCount(count);
        return tableData;
    }

    public DefaultMsg saveOrupdate(Ptype ptype) {
        DefaultMsg defaultMsg=new DefaultMsg();
        Integer count;
        if (ptype.getTid()==null){
            count=ptypeDao.save(ptype);
        }else {
            count=ptypeDao.update(ptype);
        }
        if (count==0){
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }

    public DefaultMsg delete(Integer tid) {
        DefaultMsg defaultMsg=new DefaultMsg();
        Integer delete = ptypeDao.delete(tid);
        if (delete==null){
            defaultMsg.setError("删除失败");
        }
        return defaultMsg;
    }
}
