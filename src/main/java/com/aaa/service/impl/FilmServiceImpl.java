package com.aaa.service.impl;

import com.aaa.dao.FilmDao;
import com.aaa.entity.Film;
import com.aaa.service.FilmService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmServiceImpl implements FilmService {
    @Autowired
    private FilmDao filmDao;


    public DefaultMsg checkName(String name, Integer id) {
        //如果不重复 defaultMsg success=1 如果重复 success=0
        DefaultMsg defaultMsg=new DefaultMsg();
        //新增
        if (id == null) {
            Integer count = filmDao.countByName(name);
            if (count > 0) {
                defaultMsg.setSuccess(0);
            }
        } else {
            //修改
            Integer count = filmDao.countByName(name, id);
            if (count > 0) {
                defaultMsg.setSuccess(0);
            }
        }
        return defaultMsg;
    }

    public TableData<Film> page(String keyword, String country, String director, String actors, String tid, Integer page, Integer limit) {
        TableData<Film> tableData=new TableData<Film>();
        Integer offset=(page-1)*limit;
        List<Film> data = filmDao.listPage(keyword,country,director,actors,tid,offset, limit);
        tableData.setData(data);
        Integer count = filmDao.count(keyword);
        tableData.setCount(count);
        return tableData;
    }



    public DefaultMsg saveOrUpdate(Film film) {
        DefaultMsg defaultMsg=new DefaultMsg();
        Integer count;
        if (film.getId()==null){
            count=filmDao.save(film);
        }else {
            count=filmDao.update(film);
        }


        if (count==0){
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }

    public DefaultMsg delete(Long id) {
        DefaultMsg defaultMsg=new DefaultMsg();
        Integer count=filmDao.delete(id);
        if (count==0){
            defaultMsg.setError("删除失败");
        }
        return defaultMsg;
    }

    public List<Film> listAll() {
        return filmDao.listAll();
    }
}
