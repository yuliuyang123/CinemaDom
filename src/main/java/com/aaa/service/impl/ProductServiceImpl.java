package com.aaa.service.impl;

import com.aaa.dao.ProductDao;
import com.aaa.entity.Product;
import com.aaa.service.ProductService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品管理service实现类
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    public TableData<Product> page(String keyword,Integer page, Integer limit) {
        TableData<Product> tableData = new TableData<Product>();
        //第一步，查询当前页的数据
        Integer offset = (page-1)*limit;
        List<Product> data = productDao.listPage(keyword,offset, limit);
        tableData.setData(data);
        //第二步，查询总条数
        Integer count = productDao.count(keyword);
        tableData.setCount(count);
        return tableData;
    }

    public DefaultMsg saveOrUpdate(Product product) {
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer count;
        if(product.getPid()==null){
            count=productDao.save(product);
        }else {
            count=productDao.update(product);
        }
        if(count==0){
            // defaultMsg.setSuccess(0);
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }

    public DefaultMsg delete(Long pid) {
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer count = productDao.delete(pid);
        if(count==0){
            defaultMsg.setError("删除失败");
        }
        return defaultMsg;
    }
}
