package com.aaa.service.impl;

import com.aaa.dao.HallDao;
import com.aaa.entity.Hall;
import com.aaa.service.HallService;
import com.aaa.util.DateUtil;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HallServiceImpl implements HallService {
    @Autowired
    private HallDao hallDao;
    public TableData<Hall> page(String keyword, Integer page, Integer limit) {
        TableData<Hall> tableData = new TableData<Hall>();
        //第一步，查询当前页的数据
        Integer offset = (page-1)*limit;
        List<Hall> data =hallDao.listPage(keyword,offset, limit);
        tableData.setData(data);
        //第二步，查询总条数
        Integer count = hallDao.count(keyword);
        tableData.setCount(count);
        return tableData;
    }

    public DefaultMsg saveOrUpdate(Hall hall) {
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer count;
        if(hall.getHid()==null){
            hall.setCreate_date(DateUtil.getCurrentDate());
            count=hallDao.save(hall);
        }else {
            count=hallDao.update(hall);
        }
        if(count==0){
            // defaultMsg.setSuccess(0);
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }

    public DefaultMsg delete(Long hid) {
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer count = hallDao.delete(hid);
        if(count==0){
            defaultMsg.setError("删除失败");
        }
        return defaultMsg;
    }

    public DefaultMsg checkName(String hname, Integer hid) {
//如果不重复 defaultMsg success=1 如果重复 success=0
        DefaultMsg defaultMsg=new DefaultMsg();
        //新增
        if (hid == null) {
            Integer count = hallDao.countByName(hname);
            if (count > 0) {
                defaultMsg.setSuccess(0);
            }
        } else {
            //修改
            Integer count = hallDao.countByName(hname, hid);
            if (count > 0) {
                defaultMsg.setSuccess(0);
            }
        }
        return defaultMsg;
    }
}
