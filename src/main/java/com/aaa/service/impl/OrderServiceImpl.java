package com.aaa.service.impl;

import com.aaa.dao.OrderDao;
import com.aaa.dao.OrderDetailDao;
import com.aaa.entity.Order;
import com.aaa.entity.OrderDetail;
import com.aaa.entity.Product;
import com.aaa.entity.User;
import com.aaa.service.OrderService;
import com.aaa.util.ProductUtil;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private OrderDetailDao orderDetailDao;


    public TableData<Order> page(String keyword, Integer page, Integer limit) {
        TableData<Order> tableData = new TableData<Order>();
        //第一步，查询当前页的数据
        Integer offset = (page-1)*limit;
        List<Order> data = orderDao.listPage(keyword,offset, limit);
        tableData.setData(data);
        //第二步，查询总条数
        Integer count = orderDao.count();
        tableData.setCount(count);
        return tableData;
    }


    public TableData<OrderDetail> pageDetail(String keyword, Integer page, Integer limit, Long oid) {
        TableData<OrderDetail> tableData = new TableData<OrderDetail>();
        //第一步，查询当前页的数据
        Integer offset = (page-1)*limit;
        List<OrderDetail> data = orderDetailDao.listPage(offset, limit,oid);
        tableData.setData(data);
        //第二步，查询总条数
        Integer count = orderDetailDao.count(keyword);
        tableData.setCount(count);
        return tableData;
    }


    public DefaultMsg save(List<ProductUtil> productUtils) {
        DefaultMsg defaultMsg = new DefaultMsg();
        //生成一个随机的订单号
        int orderNumber = (int)((Math.random()*9+1)*100000);
        Double total = 0.0;
        Order order = new Order();
        //订单编号
        order.setOrder_number(String.valueOf(orderNumber));
        for (ProductUtil productUtil : productUtils){
            total += productUtil.getUnit_price() * productUtil.getStock();
            OrderDetail orderDetail = new OrderDetail();
            //单价
            orderDetail.setPrice(productUtil.getUnit_price());
            //数量
            orderDetail.setStock(productUtil.getStock());
            //小计
            orderDetail.setSubtotal(productUtil.getUnit_price()* productUtil.getStock());
            //oid
            Product product = new Product();
            //商品id
            product.setPid(productUtil.getPid());
            orderDetail.setProduct(product);
            orderDetail.setOrder(order);
            Integer count = orderDetailDao.save(orderDetail);
            if(count == 0){
                defaultMsg.setError("添加失败");
                return defaultMsg;
            }
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = simpleDateFormat.format(new Date());
        //uid
        //创建时间h
        order.setCreate_date(date);
        //总金额
        order.setTotal(total);
        Integer count = orderDao.save(order);
        if(count == 0){
            defaultMsg.setError("添加失败");
        }
        return defaultMsg;
    }
}
