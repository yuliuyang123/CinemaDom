package com.aaa.service.impl;

import com.aaa.dao.IdentityDao;
import com.aaa.entity.Identity;
import com.aaa.service.IdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IdentityServiceImpl implements IdentityService {
    @Autowired
    private IdentityDao identityDao;
    public List<Identity> listAll() {
        return identityDao.listAll();
    }
}
