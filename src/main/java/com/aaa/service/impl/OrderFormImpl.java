package com.aaa.service.impl;

import com.aaa.dao.OrderFormDao;
import com.aaa.entity.Identity;
import com.aaa.entity.OrderForm;
import com.aaa.entity.TicketSales;
import com.aaa.service.OrderFormService;
import com.aaa.util.DateUtil;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class OrderFormImpl implements OrderFormService {
    @Autowired
    private OrderFormDao orderFormDao;

    public TableData<OrderForm> listAll(String keyword, Integer page, Integer limit) {
        TableData<OrderForm> tableData = new TableData<OrderForm>();
        //第一步，查询当前页的数据
        Integer offset = (page - 1) * limit;
        List<OrderForm> data = orderFormDao.listAll(keyword, offset, limit);
        tableData.setData(data);
        //第二步，查询总条数
        Integer count = orderFormDao.count(keyword);
        tableData.setCount(count);
        return tableData;
    }

    public DefaultMsg save(OrderForm orderForm) {
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer count;
        String ordeId = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        int hashCode = java.util.UUID.randomUUID().toString().hashCode();
        if (hashCode <0){
            hashCode=-hashCode;
        }
        String ticket_code = String.format("%010d", hashCode).substring(0,8);
        orderForm.setOrdeId(Long.valueOf(ordeId));
        orderForm.setTicket_code(Long.valueOf(ticket_code));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        orderForm.setBooking_time(simpleDateFormat.format(new Date()));
        Identity state = new Identity();
        state.setId(Long.valueOf(1));
        orderForm.setState(state);
         count = orderFormDao.save(orderForm);
//        count = hallDao.save(hall);
        if (count == 0) {
            // defaultMsg.setSuccess(0);
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }

    public Integer update(Long numseat, Long tid) {
        return orderFormDao.update(numseat,tid);
    }
}
