package com.aaa.service.impl;

import com.aaa.dao.LeadingUserDao;
import com.aaa.entity.LeadingUser;
import com.aaa.service.LeadingUserService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeadingUserServiceImpl implements LeadingUserService {
    @Autowired
    private LeadingUserDao leadingUserDao;
    public TableData<LeadingUser> page(String keyword, Integer page, Integer limit) {
        TableData<LeadingUser> tableData=new TableData<LeadingUser>();
        Integer offset=(page-1)*limit;
        List<LeadingUser> data=leadingUserDao.listPage(keyword,offset,limit);
        tableData.setData(data);
        Integer count=leadingUserDao.count(keyword);
        tableData.setCount(count);
        return tableData;
    }


    public DefaultMsg identity(LeadingUser leadingUser, Long lid) {
        DefaultMsg defaultMsg=new DefaultMsg();
        Integer count = leadingUserDao.integer(leadingUser,lid);
        if (count==0){
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }
}
