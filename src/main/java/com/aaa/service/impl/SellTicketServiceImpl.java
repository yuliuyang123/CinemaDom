package com.aaa.service.impl;

import com.aaa.dao.SellTicketDao;
import com.aaa.entity.TicketSales;
import com.aaa.service.SellTicketService;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SellTicketServiceImpl implements SellTicketService {
    @Autowired
    private SellTicketDao sellTicketDao;
    public TableData<TicketSales> page(Long fid,Integer page, Integer limit) {
        TableData<TicketSales> tableData = new TableData<TicketSales>();
        //第一步，查询当前页的数据
        Integer offset = (page-1)*limit;
        List<TicketSales> data =sellTicketDao.listPage(fid,offset, limit);
        tableData.setData(data);
        //第二步，查询总条数
        Integer count = sellTicketDao.count(fid);
        tableData.setCount(count);
        return tableData;
    }
}
