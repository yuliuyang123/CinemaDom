package com.aaa.service.impl;

import com.aaa.dao.MenuDao;
import com.aaa.entity.Menu;
import com.aaa.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单管理service实现类
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuDao menuDao;
    public List<Menu> listMyMenus(Long uid) {
        //第一步，查询用户能操作的 所有一级菜单
        List<Menu> oneMenus = menuDao.listOneMenus(uid);
        //第二步，遍历所有用户能操作的一级菜单
        for(Menu oneMenu : oneMenus){
            //第三步，查询用户能操作的某一个一级菜单下的二级菜单
            List<Menu> twoMenus = menuDao.listTwoMenus(uid, oneMenu.getId());
            //第四步，把用户能操作的二级菜单twoMenus放到一级菜单的集合属性中
            oneMenu.setChildren(twoMenus);
        }

        return oneMenus;
    }

    public Map<String, Object> listMiniUiMenus(Long uid) {
        Map<String,Object> map = new HashMap();
        //拼homeInfo对象
        Map<String,Object> homeInfo = new HashMap<String, Object>();
        homeInfo.put("title","首页");
        homeInfo.put("href","page/welcome-1.html?t=1");
        map.put("homeInfo",homeInfo);
        //拼logoInfo对象
        Map<String,Object> logoInfo = new HashMap<String, Object>();
        logoInfo.put("title","后台管理系统");
        logoInfo.put("image","static/layuimini-v2-onepage/images/logo.png");
        logoInfo.put("href","");
        map.put("logoInfo",logoInfo);
        //拼menuInfo
        List<Object> menuInfo = new ArrayList<Object>();
        Map<String,Object> changGuiGuanLi = new HashMap<String, Object>();
        changGuiGuanLi.put("title","常规管理");

        //拼一级菜单
        List<Menu> oneMenus = this.listMyMenus(uid);
        List<Object> caiDan = new ArrayList<Object>();
        for(Menu oneMenu : oneMenus){
            Map<String,Object> oneMenuMap = new HashMap<String, Object>();
            oneMenuMap.put("title",oneMenu.getName());
            oneMenuMap.put("href",oneMenu.getPath());
            oneMenuMap.put("target","_self");
            oneMenuMap.put("icon","fa fa-home");
            //拼二级菜单
            List<Object> ziCaiDans = new ArrayList<Object>();
            for(Menu twoMenu : oneMenu.getChildren()){
                Map<String,Object> twoMenuMap = new HashMap<String, Object>();
                twoMenuMap.put("title",twoMenu.getName());
                twoMenuMap.put("href",twoMenu.getPath());
                twoMenuMap.put("target","_self");
                twoMenuMap.put("icon","fa fa-navicon");
                ziCaiDans.add(twoMenuMap);
            }
            oneMenuMap.put("child",ziCaiDans);
            caiDan.add(oneMenuMap);
        }
        changGuiGuanLi.put("child",caiDan);
        menuInfo.add(changGuiGuanLi);
        map.put("menuInfo",menuInfo);
        return map;
    }

    public List<String> convertMenus(List<Menu> oneMenus) {
        List<String> result = new ArrayList<String>();
        //遍历一级菜单
        for(Menu oneMenu :oneMenus){
            //获取二级菜单
            List<Menu> twoMenus = oneMenu.getChildren();
            //遍历二级菜单
            for(Menu twoMenu:twoMenus){
                //获取二级菜单的命名空间  权限标识
                String namespace = twoMenu.getNamespace();
                result.add(namespace);

            }
        }

        return result;
    }

    public List<Menu> listAllByLevel() {
        List<Menu> oneMenus = menuDao.listOneMenus();
        for(Menu oneMenu : oneMenus){
            List<Menu> twoMenus = menuDao.listTwoMenus(oneMenu.getId());
            oneMenu.setChildren(twoMenus);
        }
        //System.out.println(oneMenus);
        return oneMenus;
    }

    public List<Menu> listRoleMenus(Long rid) {
        return menuDao.listRoleMenus(rid);
    }
}
