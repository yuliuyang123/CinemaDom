package com.aaa.service.impl;

import com.aaa.dao.UserDao;
import com.aaa.entity.User;
import com.aaa.entity.UserRoleVO;
import com.aaa.service.UserService;
import com.aaa.util.DateUtil;
import com.aaa.util.DefaultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    public User checkUser(User user) {
        try {
            User user1 = userDao.finByUsernameAndPassword(user);
            return user1;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public User byUid(Long uid) {

        User user = userDao.byUid(uid);
        return user;

    }



    public List<UserRoleVO> listAll(String uname, String sex, String name, Integer page, Integer limit) {
        Integer offset = (page - 1) * limit;
        return userDao.listAll(uname, sex, name, offset, limit);
    }

    public Integer numberOfEmployees() {
        return userDao.numberOfEmployees();
    }

    public List<UserRoleVO> listAllRole() {
        return userDao.listAllRole();
    }


    public DefaultMsg saveOrUpdate(User user,Long rid)  {
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer count;
        Long uid = user.getUid();
        Long u_id = null;
        if (uid == null) {
            //自动添加时间
            user.setEntrytime(DateUtil.getCurrentDate());
            count = userDao.save(user);
            if(count>0){
                User user1 = null;
                try {
                    user1 = userDao.finByUsernameAndPassword(user);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                u_id=user1.getUid();
                count = userDao.saveUserRole(u_id, rid);
                if (count == 0) {
                    throw new RuntimeException("添加用户角色失败");
                }
            }
        } else {
            count = userDao.update(user);
            //删除用户之前的角色
            count = userDao.deleteUserRoles(uid);
            if (count == 0) {
                // throw  new RuntimeException("删除用户角色失败");
            }
        }

        //添加新角色


        return defaultMsg;
    }

    public DefaultMsg update(User user) {
        DefaultMsg defaultMsg = new DefaultMsg();
        userDao.update(user);
        return defaultMsg;
    }

    public Integer saveUserRole(Long u_id, Long r_id) {
        return userDao.saveUserRole(u_id, r_id);
    }
}
