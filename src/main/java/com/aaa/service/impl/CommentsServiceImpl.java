package com.aaa.service.impl;

import com.aaa.dao.CommentsDao;
import com.aaa.entity.Comments;
import com.aaa.service.CommentsService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentsServiceImpl implements CommentsService {
    @Autowired
    private CommentsDao commentsDao;
    public TableData<Comments> page(String keyword, Integer page, Integer limit) {
        TableData<Comments> tableData=new TableData<Comments>();
        Integer offset=(page-1)*limit;
        List<Comments> data = commentsDao.listPage(keyword, offset, limit);
        tableData.setData(data);
        Integer count = commentsDao.count(keyword);
        tableData.setCount(count);
        return tableData;
    }

    public DefaultMsg delete(Integer id) {
        DefaultMsg defaultMsg=new DefaultMsg();
        Integer count=commentsDao.delete(id);
        if (count==0){
            defaultMsg.setError("删除失败");
        }
        return defaultMsg;
    }
}
