package com.aaa.service.impl;

import com.aaa.dao.FtypeDao;
import com.aaa.entity.Ftype;
import com.aaa.service.FtypeService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class FtypeServiceImpl implements FtypeService {
    @Autowired
    private FtypeDao ftypeDao;
    public List<Ftype> listAll() {
        return ftypeDao.listAll();
    }

    public TableData<Ftype> page(String keyword, Integer page, Integer limit) {
        TableData<Ftype> tableData=new TableData<Ftype>();
        Integer offset=(page-1)*limit;
        List<Ftype> data=ftypeDao.listPage(keyword,offset,limit);
        tableData.setData(data);
        Integer count=ftypeDao.count(keyword);
        tableData.setCount(count);
        return tableData;    }

    public DefaultMsg checkName(String name, Integer tid) {
        //如果不重复 defaultMsg success=1 如果重复 success=0
        DefaultMsg defaultMsg=new DefaultMsg();
        //新增
        if (tid == null) {
            Integer count = ftypeDao.countByName(name);
            if (count > 0) {
                defaultMsg.setSuccess(0);
            }
        } else {
            //修改
            Integer count = ftypeDao.countByName(name, tid);
            if (count > 0) {
                defaultMsg.setSuccess(0);
            }
        }
        return defaultMsg;
    }

    public DefaultMsg saveOrUpdate(Ftype ftype) {
        DefaultMsg defaultMsg=new DefaultMsg();
        Integer count;
        if (ftype.getTid()==null){
            count=ftypeDao.save(ftype);
        }else {
            count=ftypeDao.update(ftype);
        }

        if (count==0){
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }



}
