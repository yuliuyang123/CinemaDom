package com.aaa.service.impl;

import com.aaa.dao.StatisticsDao;
import com.aaa.entity.Statistics;
import com.aaa.service.StatisticsService;
import com.aaa.util.DefaultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatisticsServiceImpl implements StatisticsService {
    @Autowired
    private StatisticsDao statisticsDao;
    public Integer saveOrUpdate(Statistics statistics) {
        Integer integer = statisticsDao.listByFid(statistics.getFilm().getId());
        Integer count;
        if(integer==0){
            count=statisticsDao.save(statistics);
        }else {
            count=statisticsDao.update(statistics);
        }
        return count;
    }

    public List<Statistics> listTop10() {
        return statisticsDao.listTop10();
    }
}
