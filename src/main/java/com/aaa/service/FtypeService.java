package com.aaa.service;

import com.aaa.entity.Ftype;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

import java.util.List;

public interface FtypeService {
    List<Ftype> listAll();
    TableData<Ftype> page(String keyword, Integer page, Integer limit);
    public DefaultMsg checkName(String name,Integer tid);

    DefaultMsg saveOrUpdate(Ftype ftype);

}
