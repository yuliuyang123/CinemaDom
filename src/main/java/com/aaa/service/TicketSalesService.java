package com.aaa.service;

import com.aaa.entity.TicketSales;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

public interface TicketSalesService {
    /**
     * 分页查询
     * @param page 第几页
     * @param limit 每页多少条
     * @return
     */
    public TableData<TicketSales> page(String keyword, Integer page, Integer limit);

    DefaultMsg save(TicketSales ticketSales);

    //查询所有拍片是否重复

    DefaultMsg checkSales(String opening_time,String break_time,Long hid);


    DefaultMsg delete(Long tid);
}
