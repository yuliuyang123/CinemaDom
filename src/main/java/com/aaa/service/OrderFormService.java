package com.aaa.service;

import com.aaa.entity.OrderForm;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;


public interface OrderFormService {
    TableData<OrderForm> listAll(String keyword, Integer page, Integer limit);
    DefaultMsg save(OrderForm orderForm);
    Integer update(Long numseat,Long tid);
}
