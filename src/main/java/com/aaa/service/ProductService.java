package com.aaa.service;

import com.aaa.entity.Product;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

/**
 * 商品管理service接口
 */
public interface ProductService {


    /**
     * 分页查询
     * @param page 第几页
     * @param limit 每页多少条
     * @return
     */
    public TableData<Product> page(String keyword,Integer page,Integer limit);


    /**
     * 保存或者修改商品信息
     * @param product
     * @return
     */
    public DefaultMsg saveOrUpdate(Product product);


    /**
     *  根据商品id 删除商品信息
     * @param pid
     * @return
     */
    public DefaultMsg delete(Long pid);
}
