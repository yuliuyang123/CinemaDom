package com.aaa.service;

import com.aaa.entity.Film;
import com.aaa.entity.User;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

import java.util.List;

public interface FilmService {
    public DefaultMsg checkName(String name,Integer id);
    public TableData<Film> page(String keyword,String country,String director,String actors, String tid, Integer page,Integer limit);

    public DefaultMsg saveOrUpdate(Film film);
    public DefaultMsg delete(Long id);
    public List<Film> listAll();
}
