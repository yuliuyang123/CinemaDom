package com.aaa.service;

import com.aaa.entity.Comments;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

public interface CommentsService {
    public TableData<Comments> page(String keyword,Integer page,Integer limit);

    public DefaultMsg delete(Integer id);
}
