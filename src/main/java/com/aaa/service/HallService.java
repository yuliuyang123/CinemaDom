package com.aaa.service;

import com.aaa.entity.Hall;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

public interface HallService {

    /**
     * 分页查询
     * @param page 第几页
     * @param limit 每页多少条
     * @return
     */
    public TableData<Hall> page(String keyword, Integer page, Integer limit);


    /**
     * 保存或者修改影厅信息
     * @param hall
     * @return
     */
    public DefaultMsg saveOrUpdate(Hall hall);


    /**
     *  根据商品id 删除商品信息
     * @param hid
     * @return
     */
    public DefaultMsg delete(Long hid);


    public DefaultMsg checkName(String hname,Integer hid);

}
