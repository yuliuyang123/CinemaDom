package com.aaa.service;

import com.aaa.entity.User;
import com.aaa.entity.UserRoleVO;
import com.aaa.util.DefaultMsg;

import java.util.List;


public interface UserService {
    /**
     * 校验用户名密码是否正确
     * @param user
     * @return
     */
    User checkUser(User user);
    User byUid(Long uid) throws Exception;
    DefaultMsg update(User user);
    List<UserRoleVO> listAll(String uname,String sex,String name,Integer page , Integer limit);
    Integer  numberOfEmployees();
    List<UserRoleVO> listAllRole();

    Integer saveUserRole(Long u_id,Long r_id);
    /**
     * 保存或者修改用户信息
     * @param user
     * @return
     */
     DefaultMsg saveOrUpdate(User user ,Long rid) throws Exception;
}
