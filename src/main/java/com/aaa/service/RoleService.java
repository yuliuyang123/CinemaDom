package com.aaa.service;

import com.aaa.entity.Role;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface RoleService {
    /**
     * 分页查询
     * @param page 第几页
     * @param limit 每页多少条
     * @return
     */
    public TableData<Role> page(String keyword, Integer page, Integer limit);
    /**
     * 保存或者修改角色信息
     * @param role
     * @return
     */
    public DefaultMsg saveOrUpdate(Role role, String rids);

    /**
     * 查询所有的角色信息
     * @return
     */
    public List<Role> listAll();

    /**
     * 查询用户具有的角色
     * @param uid
     * @return
     */
    public List<Role> listUserRoles(Long uid);



    public DefaultMsg checkName(String name,Integer id);

}
