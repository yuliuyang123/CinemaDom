package com.aaa.service;

import com.aaa.entity.Film;
import com.aaa.util.DefaultMsg;
import com.aaa.util.TableData;

public interface MovieService {

    public TableData<Film> page(String keyword, Integer page, Integer limit);

    public DefaultMsg saveOrUpdate(Film movie);

    public DefaultMsg delete(Long id);
}
