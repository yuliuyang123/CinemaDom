package com.aaa.service;

import com.aaa.entity.Mtype;

import java.util.List;

public interface MtypeService {
    List<Mtype> listAll();
}
